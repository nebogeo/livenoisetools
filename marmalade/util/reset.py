#!/usr/bin/env python

import sys
import osc

oscport = int(sys.argv[1])
osc.Message("/reset").sendlocal(oscport)
