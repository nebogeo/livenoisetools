// Copyright (C) 2005 Dave Griffiths
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "Heap.h"
#include "Registers.h"

Heap::Heap(unsigned int size) :
m_Size(size)
{
	// fill mem with zero
	for (unsigned int i=0; i<m_Size; i++)
	{
		m_Heap.push_back(new Num(0));
	}	
}

Heap::~Heap()
{
	for (vector<Data*>::iterator i=m_Heap.begin(); i!=m_Heap.end(); i++)
	{
		delete *i;
	}
}

void Heap::Clear()
{
	for (unsigned int i=0; i<m_Size; i++)
	{
		Write(i,0);
	}
}

Data* Heap::ReadRaw(unsigned int addr)
{	
	if (addr<m_Size) return m_Heap[addr];
	return NULL;
}
	
float Heap::ReadNum(unsigned int addr)
{
	if (addr<m_Size)
	{
		Data* d=m_Heap[addr];
		if (d->Type()=='n') return static_cast<Num*>(d)->Value; 
	}
	return 0;
}

string Heap::ReadStr(unsigned int addr)
{
	if (addr<m_Size)
	{
		Data* d=m_Heap[addr];
		if (d->Type()=='s') return static_cast<Str*>(d)->Value; 
	}
	return "";
}

void Heap::WriteRaw(unsigned int addr,Data *s)
{
	if (addr<m_Size)
	{
		delete m_Heap[addr];
		m_Heap[addr]=s;
	}
}

void Heap::Write(unsigned int addr,float s)	
{
	if (addr<m_Size)
	{
		delete m_Heap[addr];
		m_Heap[addr]=new Num(s);
	}
}

void Heap::Write(unsigned int addr,string s)
{
	if (addr<m_Size)
	{
		delete m_Heap[addr];
		m_Heap[addr]=new Str(s);
	}
}

char Heap::Type(unsigned int addr)
{	
	if (addr<m_Size)
	{
		Data* d=m_Heap[addr];
		return d->Type();
	}
	return '0';
}
