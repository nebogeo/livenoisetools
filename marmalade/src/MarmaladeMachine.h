// Copyright (C) 2005 Dave Griffiths
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <map>
#include <deque>
#include <string>
#include <vector>
#include <iostream>
#include "Opcodes.h"
#include "Registers.h"
#include "Data.h"
#include "Heap.h"
#include "Stack.h"
#include "DebugInfo.h"

using namespace std;

#ifndef ORANGE_MACHINE
#define ORANGE_MACHINE

const unsigned int HEAP_SIZE = 2048;
const unsigned int STACK_SIZE = 2048;

class MarmaladeMachine
{
public:
	MarmaladeMachine(int CodeStart=0x50);
	~MarmaladeMachine();
	
	void Reset();
	void LoadAsm(const string &filename);
	void Asm(const string &code);
	void Execute();
	Data *Peek(unsigned int addr);
	void Poke(unsigned int addr, Data* s);
	
	class MsgDesc
	{
	public:
		MsgDesc();
		~MsgDesc();
		void Clear();
		string Path;
		vector<Data*> Args;
	};	
	
	string GetOutPort() { return m_Heap.ReadStr(OSC_OUT_PORT); }
	bool PollMessage(MsgDesc &Message);
	DebugInfo GetDebugInfo();
	
private:

	int CurrentLine();
	void IncPC();

	// operations
	void psh();
	void pop();
	void snd();
	void osc();
	void jmp();
	void jmz();
	void equ();
	void cal();
	void ret();
	void rdf();
	void wrf();
	void rdi();
	void wri();
	void inc();
	void dec();
	void add();
	void sub();
	void mul();
	void div();

	deque<int> m_PCStack;
	Stack m_Stack;
	Heap m_Heap;
	int m_HeapTop;
	int m_CodeStart;		
	bool m_MsgReady;
	MsgDesc m_MsgDesc;
	map<int,int> m_PCLineMap;
	
};

#endif
