Introduction

This document describes a text based method of composing music, and outlines various methods for generating and controlling melody.

Overview

The textual composition system described here comprises three seperate levels.

1. Text score language
A simple text character based score system that is interpreted a character at a time.

2. Lsystem score generator
A rule based production grammar that generates score code

3. Genetic programming
A genetic breeder for Lsystem grammars that generate score code

---------------------------------------------------------------------------

Lindenmayer Systems

Lindenmayer Systems were invented in 1968 by biologist Aristid Lindenmayer, and are usually used to describe natural shapes which exhibit fractal geometry, such as plants.

Lsystems (as they are often shortened to) are a type of formal grammar, which is a precice mathmatical descriptions of a language. Formal grammars can either be used as proofs that a set of strings are contained within a system, or used the other way around, as production rules to create strings.

Lsystems are a form of the latter type of formal grammar, that allow us to build strings using the Lsystem as a description. Formal grammars consist of axioms, starting strings, and production rules - in Lsystems these are replacements from one token to another.

Simple LSystem in action:

Our Lsystem formal grammar:
Starting string (axiom) : "A"
Production rule 1 : Replace A with B
Production rule 2 : Replace B with AB

If we apply the rules multiple times, the string will expand, or grow as follows:

Generation 0 : A
Generation 1 : B
Generation 2 : AB
Generation 3 : BAB
Generation 4 : ABBAB
Generation 5 : BABABBAB
Generation 6 : ABBABBABABBAB

There are several aspects of the resultant strings that are interesting. Firstly, they contain quite a lot of emergent complexity, given the simple nature of the rules used to generate them. Secondly, they contain self similarity, which is why they tend to be useful for generating plant forms. This self similarity and nested behaviour is also beneficial for describing and generating musical note data.

Musical representation

The way an Lsystems rules are interpreted is entirely seperate to the grammar that was used to create them. In traditional Lsystems, they are used to parent and position three dimensional models to make a nice tree - but we can use them for anything.

A simple musical score language

o : play a note
+ : current note pitch up one semitone
- : current note pitch down one semitone

So a score like this:

o+o+o---o+o+o----o+o+o---o+o+o

If you conside a sequencer playing the score one note at a time, it would be represented in a traditional piano roll sequencer something like this:

  o  o
 o  o   o  o
o  o   o  o
      o  o
  
So, we could use Lsystems rules to create this in the following way:

Axiom: A---A----A---A
Rule 1: Substitute A with: o+o+o

Things start to get more interesting when we repeat the process, as in the first example. This allows use to use circular definitions in the rules to create recursive patterns.

Axiom: A
Rule 1: Substitute A with: o-B+B
Rule 2: Substitute B with: AoA

Generation 0 : A
Generation 1 : o-B+B
Generation 2 : o-AoA+AoA
Generation 3 : o-o-B+Boo-B+B+o-B+Boo-B+B
Generation 4 : o-o-AoA+AoAoo-AoA+AoA+o-AoA+AoAoo-AoA+AoA

Which in our piano roll editor, would look something like this:

o       o ooo o
 o ooo o o   o
  o   o
  
Let's run it for one more generation...

Generation 5 : o-o-o-B+Boo-B+B+o-B+Boo-B+Boo-o-B+Boo-B+B+o-B+Boo-B+B+o-o-B+
Boo-B+B+o-B+Boo-B+Boo-o-B+Boo-B+B+o-B+Boo-B+B

o               o   ooooo   ooo
 o   ooooo   ooo ooo     ooo
  ooo     ooo
  
(I think this is all correct, but I'm doing it by hand: todo - check :))

So you can see the great amount of complexity that arises from such simple rule systems. After a bit of practice you start to learn what different sorts of rules will sound like, but it's a very empirical and intuitive process.

Note: the document refers to melody, but these rules can generate percussion patterns too, if you consider note frequencies to map to different percussion instruments.

The Lsystem vs the score code

A very important thing to state here is that there are two seperate systems going on here. The musical score system (how the symbols are interpreted) is _not_ the Lsystem. The Lsystem comprises the rules used to create code that is interpreted as music. The musical score is really just a side effect of the Lsystem growing it's string. In other words, the Lsystem does not know or care what the symbols mean, it just produces strings of them.

A better musical representation

The previous examples used a very basic form of notation, there are many musical things it can't represent, so we'll extend it a little.

Firstly the prevous system only dealt with notes and pitch changes, no rests or simultaneous notes, so no chords. If we incorporate a new symbol "." as a rest, we can change the rules a bit.

Now the sequencer will only rest when it reads a "." in the score, so we can trigger notes at the same time.

o.o.o.o

Will play four notes in sequence. This however:

o+o

will now play the two notes at the same time, one higher by a semitone than the other.
Another symbol we'll add at this point is the "!" which resets the current pitch to a predefined root value (this value comes from out of this system, but for now we will take it to be middle C4) So now we can start talking about things in a more musical language:

!o++++o+++o

Will play a C major chord (consisting of C4,E4 and G4 where the root note is C4)

!o++++o+++o...!+o++++o+++o...

Will play C major chord, rest for 3 beats and then play C# major (C#,E# and G#)
Also, we'd like to be able to pan around the stereo field - so we also add three more symbols for that:

'/' : pan right
'\' : pan left
'|' : center pan

The pan position works in the same way as the pitch, in that this:

/o./o./o./o./o./o./o./o.

Will play a series of note all panned slightly right of each other. When it reaches full right pan, the note will flip to full left pan, so the note will appear to continuously move from left to right.

Genetic Programming: an Lsystem breeder

Genetic programming is a method of automatic programming, where you grow a program by it's suitablility to fix a certain problem. Firstly a population of candidate programs are generated (usually randomly) and scored based on how well they solve the problem. The best ones are picked and used to generate a new population of candidates. The process continues until a suitable program results.

The genetic programming discussed here is a somewhat specialised version of this. Firstly the guidance of which program is more suitable is driven by the player, so there is no automatic selection. Also in traditional GP there are usually hundreds of programs in a population - here, to simplify the choice for a human player, there are only 4.

The genetic programming breeder can also pick up an lsystem originally written by the player and evolve it further, where it may agan be further modified by the player.

First we write a simple scorecode lsystem that creates a nice melody:

!1
2---2---2---2---2
o.+o.+o.2.2

Which after 2 generations expands to:

!o.+o.+o.o.+o.+o.2.2.o.+o.+o.2.2---o.
+o.+o.o.+o.+o.2.2.o.+o.+o.2.2---o.+o.
+o.o.+o.+o.2.2.o.+o.+o.2.2---o.+o.+o.
o.+o.+o.2.2.o.+o.+o.2.2---o.+o.+o.o.+
o.+o.2.2.o.+o.+o.2.2

The Lsystem breeder can now be used to generate mutated versions of this parent program.
There are now 3 mutated versions of this melody to audition, namely:

!o.+o-+4.o.+o-+4.2.\.\---o.+o-+4.o.+o
-+4.2.\.\---o.+o-+4.o.+o-+4.2.\.\!|-o
.+o-+4.o.+o-+4.2.\.\-.-o.+o-+4.o.+o-+
4.2.\.\

!\.+o.+o.\.+o.+o.2.2.\.+o.+o.2.2---<-
--\.+o.+o.\.+o.+o.2.2.\.+o.+o.2.2---\
.+o.+o.\.+o.+o.2.2.\.+o.+o.2.2+--\.+o
.+o.\.+o.+o.2.2.\.+o.+o.2.2

!o.+oo.+o1+o.4.2---o.+o1+o.4.2--.o.+o
1+o.4.2---o.+o1+o.4.2---o.+o1+o.4.2+o
.4.o.+o1+o.4.2---o.+oo.+o1+o.4.2---o.
+o1+o.4.2--.o.+o1+o.4.2---o.+o1+o.4.2
---o.+o1+o.4.2+o.4.o.+o1+o.4.2--.o.+o
o.+o1+o.4.2---o.+o1+o.4.2--.o.+o1+o.4
.2---o.+o1+o.4.2---o.+o1+o.4.2+o.4.o.
+o1+o.4.2---o.+oo.+o1+o.4.2---o.+o1+o
.4.2--.o.+o1+o.4.2---o.+o1+o.4.2---o.
+o1+o.4.2+o.4.o.+o1+o.4.2---o.+oo.+o1
+o.4.2---o.+o1+o.4.2--.o.+o1+o.4.2---
o.+o1+o.4.2---o.+o1+o.4.2+o.4.o.+o1+o
.4.2

Whether these are good or bad is entirely up to you, patterns can be auditioned or selected to make a new generation. The idea is to throw caution to the wind and play it live, like an
instrument.

When you one that's cool (perhaps the big long one above) you can edit the code responsible.

!1
2---2--.2---2---2
o.+o1+o.4.2

All this mutation has really done to make the result much longer, is insert a
1 in the second rule, meaning there is much more recursion going on each generation - making a much longer score.
