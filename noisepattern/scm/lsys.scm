;----------------------------------------------------------------------------
; an lsystem looks like:
; '("axiom" (("1" "rule1") ("1" "rule2")))

(define LSYS_MAX_STRING_SIZE 2048)

(define lsys-rule-sr
	(lambda (char rule)
		(if (char=? char (string-ref (car rule) 0))
			(car (cdr rule))
			#f)))

(define lsys-rules-sr
	(lambda (char rules)
		(let ((ret (lsys-rule-sr char (car rules))))
		(if (string? ret)
			ret             ; found a rule that matches, return now
			(if (eq? (cdr rules) '())
				#f
				(lsys-rules-sr char (cdr rules)))))))

(define lsys-sr
	(lambda (str rules pos result)
		(cond ((> (string-length result) LSYS_MAX_STRING_SIZE)
				result)                 ; return the string now if it's got too long now
				((let ((ret (lsys-rules-sr (string-ref str pos) rules)))
					(if (string? ret)
						(set! result (string-append result ret))
						(set! result (string-append result (string (string-ref str pos))))))

					(if (>= (+ pos 1) (string-length str))
						result
						(lsys-sr str rules (+ pos 1)  result))))))

(define ls-generate
	(lambda (str rules n)
		(set! str (lsys-sr str rules 0 ""))
		(if (< n 1)
			str
			(ls-generate str rules (- n 1)))))

(define ls-process
	(lambda (generations ls)
		(let ((str (car ls))) ; axiom is first string in the list
			(set! str (ls-generate str (car (cdr ls)) generations))
			str)))
			
			
			
