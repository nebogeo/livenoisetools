// Copyright (C) 2003 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <unistd.h>
#include <sys/time.h>
#include <spiralcore/NoteTable.h>
#include <spiralcore/Trace.h>
#include "NoisePattern.h"

float NoisePattern::m_TimeBuffer=0.5f;
float NoisePattern::m_TimeLatency=2.0f;

static const int MAX_EVENTS_PER_TICK = 4096;

NoisePattern::NoisePattern(OSCServer *server) :
m_NoteDebug(false),
m_Started(false),
m_Server(server),
m_GlobalBeatTime(1.0f/(120/60.0f)), // 120 bpm
m_QuantiseFreq(false)
{
	pthread_create(&m_SeqThread,NULL,(void*(*)(void*))Run,this);
}

NoisePattern::~NoisePattern()
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Audio thread side

void NoisePattern::Run(NoisePattern *Context)
{		
	while(1)
	{		
		Context->ProcessCommands();
		Context->Process();
		// sleep for half a buffer length (this is all guesswork)
		usleep(705);
	}
}

void NoisePattern::ProcessCommands()
{
	CommandRingBuffer::Command cmd;
	while (m_Server->Get(cmd))
	{
		string name = cmd.Name;
				
		if (name=="/modify")
		{
			map<PatternID,Pattern*>::iterator i=m_PatternMap.find(cmd.GetInt(0));
			if (i!=m_PatternMap.end())
			{
				i->second->Modify(cmd.GetString(1),cmd.GetFloat(2));
			}
			else
			{
				Trace(RED,BLACK,"Pattern %d not found",cmd.GetInt(0));
			}
		} 
		else if (name=="/modifystr")
		{
			map<PatternID,Pattern*>::iterator i=m_PatternMap.find(cmd.GetInt(0));
			if (i!=m_PatternMap.end())
			{
				i->second->Modify(cmd.GetString(1),cmd.GetString(2));
			}
			else
			{
				Trace(RED,BLACK,"Pattern %d not found",cmd.GetInt(0));
			}
		}
		else if (name=="/remove")	
		{
		}  			
		else if (name=="/swap")
		{
			/*unsigned int A=(unsigned int)cmd.GetInt(0);
			unsigned int B=(unsigned int)cmd.GetInt(1);

			int posa=-1;
			int posb=-1;

			for(unsigned int i=0; i<m_PatternOrder.size(); i++) 
			{
				if (m_PatternOrder[i]==A) posa=i;
				if (m_PatternOrder[i]==B) posb=i;
			}

			if (posa!=-1 && posb!=-1)
			{
				PatternID temp=m_PatternOrder[posa];
				m_PatternOrder[posa]=m_PatternOrder[posb];
				m_PatternOrder[posb]=temp;
			}*/
		}  			
		else if (name=="/notequantise")
		{
			m_QuantiseFreq=(bool)cmd.GetInt(0);
		}  			
		else if (name=="/loadtuning")
		{
			m_Tuna.Open(cmd.GetString(0));
			Trace(GREEN,BLACK,"using: %s",m_Tuna.GetDescription().c_str());
			//m_Tuna.Print();
		} 			
		else if (name=="/new")
		{
			map<PatternID,Pattern*>::iterator i=m_PatternMap.find(cmd.GetInt(0));
			if (i==m_PatternMap.end())
			{				
				 Pattern *newpat = new Pattern(&m_Tuna);
				 newpat->Modify("Beat",m_GlobalBeatTime);				 
				 m_PatternMap[cmd.GetInt(0)]=newpat;
				 m_PatternOrder.push_back(cmd.GetInt(0));
			}	 
		}
		else if (name=="/muteall")
		{
			for (map<PatternID,Pattern*>::iterator i = m_PatternMap.begin(); i!=m_PatternMap.end(); i++)
			{
				i->second->Modify("Playing", 0);
			}
		}
		else if (name=="/cascadetop")
		{
			unsigned int id = cmd.GetInt(0);
			for (list<PatternID>::iterator i = m_PatternOrder.begin(); i!=m_PatternOrder.end(); i++)
			{
				if (*i==id)
				{
					m_PatternOrder.erase(i);
					m_PatternOrder.push_front(id);
					m_PatternMap[id]->Modify("Playing", 1);
					break;
				}
			}
		}
		else if (name=="/notedebug")
		{      
			m_NoteDebug=cmd.GetInt(0);
		}    			
		else if (name=="/settimebuffer")
		{      
			m_TimeBuffer=cmd.GetFloat(0);
		}    			
		else if (name=="/settimelatency")
		{      
			m_TimeLatency=cmd.GetFloat(0);
		}    			
		else if (name=="/add_dest_address")
		{
			m_OscAddresses.push_back(lo_address_new(NULL, cmd.GetString(0)));
		}
		else if (name=="/add_dest_url")
		{
			m_OscEventDestination = cmd.GetString(0);
		}
		else if (name=="/filter_pass_note")
		{      
			m_FilterMode=FILTER;
		}  
		else if (name=="/filter_remove_note")
		{      
			m_FilterMode=UNFILTER;
		}  
		else if (name=="/filter_no_notes")
		{      
			m_Tuna.ClearFilter();
		}  
		else if (name=="/filter_all_notes")
		{      
			m_Tuna.FillFilter();
		}  
		else if (name=="/clear")
		{      
			for (map<PatternID,Pattern*>::iterator i=m_PatternMap.begin();
				i!=m_PatternMap.end(); i++)
			{
				delete i->second;
			}
			m_PatternMap.clear();
			m_PatternOrder.clear();
			m_OscAddresses.clear();
		}  
		else if (name=="/sync")
		{
			Time synctime(cmd.GetInt(0),cmd.GetInt(1));
			float bt = 1.0f/(cmd.GetFloat(2)/60.0f);
			bool resetall=false;
			if (bt != m_GlobalBeatTime) resetall=true;
			m_GlobalBeatTime=bt;
		
			if (synctime>m_CurrentTime) 
			{
				Time future(m_CurrentTime);
				future+=30;
				
				if (synctime<future) 
				{
					if (!m_PatternMap.empty())
					{
						// reset the top pattern
						m_PatternMap[*m_PatternOrder.begin()]->AddReset(synctime);
						
						// set the top pattern's beat (convert to beatlength from bpm)
						for (map<PatternID,Pattern*>::iterator i=m_PatternMap.begin();
							i!=m_PatternMap.end(); ++i)
						{
							i->second->Modify("Beat",m_GlobalBeatTime);
							if (resetall) i->second->AddReset(synctime);
						}
					}
				}
				else
				{
					cerr<<"sync arrived over 30 seconds early"<<endl;
				}
			}
			else
			{
				cerr<<"sync arrived "<<synctime.GetDifference(m_CurrentTime)<<" seconds too late"<<endl;
			}
		}
		else if (name=="/bpm")
		{
			m_GlobalBeatTime = 1.0f/(cmd.GetFloat(0)/60.0f);			
			for (map<PatternID,Pattern*>::iterator i=m_PatternMap.begin();
			i!=m_PatternMap.end(); ++i)
			{
				i->second->Modify("Beat",m_GlobalBeatTime);
			}
		}
		else  			
		{
			cerr<<name<<" not understood by noisepattern"<<endl;
		}
	}
}

void NoisePattern::Process()
{
	// time works like this...
	//                                 
	//  m_CurrentTime <----------------> m_SeqFrom <------------------>             	
	//                   m_TimeBuffer                  m_TimeBuffer
	//                        
	//  m_TimeBuffer could adapt predictivly so that the next m_SeqFrom will hopefully 
	//  be enough into the future for the events to get to their destination.
	
	m_CurrentTime.SetToNow();
	m_CurrentTime+=m_TimeLatency;
	
	// first time, or late?
	if (m_SeqFrom.IsEmpty() || m_SeqFrom<m_CurrentTime)
	{
		if (!m_SeqFrom.IsEmpty()) Trace(RED,YELLOW,"way too late!, bump...");

		m_SeqFrom=m_CurrentTime;
		m_SeqFrom+=m_TimeBuffer;
	}

	// too early?
	// throttle the processing if we are more than two buffers ahead
	if (m_SeqFrom.GetDifference(m_CurrentTime) > m_TimeBuffer*2)
	{
		//Trace(GREEN,BLUE,"too early, skipping... :)");
		return;
	}
		
	//get events from patterns
	vector<Event> Events;
	
	// evaluate patterns in stated order 
	for (list<PatternID>::iterator o=m_PatternOrder.begin(); o!=m_PatternOrder.end(); o++)
	{
		map<PatternID,Pattern*>::iterator i=m_PatternMap.find(*o);
		if (i!=m_PatternMap.end())
		{
			i->second->DoSlice(m_SeqFrom,m_TimeBuffer,Events);
		}
		else
		{
			Trace(RED,BLACK,"pattern order mismatch with patternmap!!");
		}
	}
	
	m_SeqFrom+=m_TimeBuffer;
	
	//cerr<<"-- events ----------------------------"<<endl;
	
	// send all the events
	int count=0;
	for (vector<Event>::iterator i=Events.begin();
		i!=Events.end(); i++)
	{
		// keep down network traffic, message events shouldn't be needed
		// externally anyway... yet?...
		if (i->Message=='o' || i->Message=='O' || i->Message=='x') 
		{
			float outfreq=i->Frequency;
			float outslidefreq=i->SlideFrequency;
			
			if (m_QuantiseFreq)
			{
				outfreq=m_Tuna.GetQuantised(i->Frequency);
				outslidefreq=m_Tuna.GetQuantised(i->SlideFrequency);
			}
			
			if (m_FilterMode!=OFF)
			{
				if (m_FilterMode==FILTER) m_Tuna.FilterClosest(outfreq); 
				if (m_FilterMode==UNFILTER) m_Tuna.UnfilterClosest(i->Frequency); 
				m_FilterMode=OFF;
			}
			
			for (vector<lo_address>::iterator ai=m_OscAddresses.begin();
			     ai!=m_OscAddresses.end(); ai++)
			{				
				lo_send(*ai, m_OscEventDestination.c_str(), "iiiffffii", 
					i->TimeStamp.Seconds,
					i->TimeStamp.Fraction,
					i->ID, 
					outfreq, 
					outslidefreq, 
					i->Volume,  
					i->Pan,
					i->Message,
					i->NoteNum);
			}
			
			count++;
			if (count>MAX_EVENTS_PER_TICK) 
			{
				Trace(RED,BLUE,"flooded with events, gonna drop some this tick");
				break;
			}
		}
		
		if (m_NoteDebug) 
		{
			cerr<<"sent "<<m_OscEventDestination<<": "<<"freq:"<<i->Frequency<<" vol:"<<i->Volume<<" ID:"<<i->ID<<endl;
			i->TimeStamp.Print();
		}
	}
}



