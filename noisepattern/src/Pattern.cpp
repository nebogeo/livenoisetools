// Copyright (C) 2003 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <math.h>
#include <iostream>
#include "Pattern.h"

static const int MAX_EVENTS_PER_TICK=4;
static const unsigned int MAX_NOTESTACK=50;

Pattern::Pattern(Tuna *t): 
m_Tuna(t),
m_Beat(1),
m_BeatMult(1),
m_Midi(1),
m_Position(0),
m_Note(m_RootNote),
m_RootNote(23),
m_Pan(0),
m_PanDistance(0.1),
m_PanAngle(0),
m_Playing(true)
{
	m_NoteStack.push_front(m_RootNote);
}

Pattern::~Pattern()
{
}
void Pattern::DoSlice(Time Start, float TimeSlice, vector<Event> &EventVec)
{	
	// just do this timeslice - used to do all upto and including 
	// this timeslice, this means we can skip patterns without them 
	// freaking out when they are restarted.
	if (m_NextTickTime.IsEmpty()) m_NextTickTime=Start;
	
	// if there is no time slice
	if (TimeSlice<=0) return;
	
	// take a sneaky peek at the existing messages
	for (vector<Event>::iterator i=EventVec.begin(); i!=EventVec.end(); i++)
	{
		// if it's a message event
		if (i->Message!=0)
		{
			// there is a reset message in this load of events
			// we only deal with one reset per time slice (the last?) so
			// this will not always act as expected with small bar lengths 
			// and long sequecer poll times...

			// reset only this pattern
			if (i->Message=='<')
			{ 
				m_ResetTimes.push_back(i->TimeStamp);
				i->Message='X'; // prevents passing on to subsequent patterns
			}

			// fallthrough reset
			if (i->Message=='^')
			{ 
				m_ResetTimes.push_back(i->TimeStamp);
			}
		}
	}
		
	Time EndTime=Start;
	EndTime+=TimeSlice; // end of this timeslice
	
	// add the events for each tick in this time slice
	while (m_NextTickTime<EndTime)
	{		
		Tick(m_NextTickTime,EventVec);
		// ok so it's a beat divide, but there you go
		m_NextTickTime+=(m_Beat/m_BeatMult);
		
		// deal with a reset, if it's time...
		for (vector<Time>::iterator i=m_ResetTimes.begin(); i!=m_ResetTimes.end(); i++)
		{
			if(!i->IsEmpty() && m_NextTickTime>=*i)
			{
				Reset();
				m_NextTickTime=*i;
				m_ResetTimes.erase(i);
				break;
			}		
		}
	}
}

void Pattern::Tick(const Time &TimeStamp, vector<Event> &EventVec)
{				
	if (m_Score.size()==0) return;
	if (m_Position>=(int)m_Score.size()) Reset();
	if (m_Position<0) return; // waiting for a reset...
	
	bool finished=false;
	bool abort=false;
	bool dupenote=false; // to prevent sending dupe notes
	int notecount=0;
	bool buildingnote=false;
	bool accent=false;
	Event cur;	
	
	while(!finished)
	{	
		switch(m_Score[m_Position])
		{
			case '+': 
			{
				*m_NoteStack.begin()=*m_NoteStack.begin()+1; 
				//if (*m_NoteStack.begin()>100) *m_NoteStack.begin()=1;
				dupenote=false;
			}
			break;
			case '-': 
			{
				*m_NoteStack.begin()=*m_NoteStack.begin()-1; 
				//if (*m_NoteStack.begin()<1) *m_NoteStack.begin()=1;
				dupenote=false; 
			} 
			break;
			case '!': *m_NoteStack.begin()=m_RootNote; dupenote=false; break;
			case '\\': m_PanAngle-=m_PanDistance; m_Pan=sin(m_PanAngle); break;
			case '/': m_PanAngle+=m_PanDistance; m_Pan=sin(m_PanAngle); break;
			case '|': m_PanAngle=0; m_Pan=0; break;
			case '.': 
			{
				if (buildingnote && !dupenote && m_Playing && notecount<MAX_EVENTS_PER_TICK)
				{
					//cur.SlideFrequency=m_RootFreq*pow(2.0f,*m_NoteStack.begin()/(float)m_NotesPerOctave);
					cur.SlideFrequency=m_Tuna->GetNote(*m_NoteStack.begin());
					EventVec.push_back(cur);
					dupenote=true; 
					notecount++;
				}
				
				finished=true; 
			}break;
			case 'o' : // fallthrough
			case 'O' : 
			case 'x' : 
			{
				if (notecount<MAX_EVENTS_PER_TICK)
				{
					if (buildingnote && !dupenote && m_Playing)
					{
						//cur.SlideFrequency=m_RootFreq*pow(2.0f,*m_NoteStack.begin()/(float)m_NotesPerOctave);
						cur.SlideFrequency=m_Tuna->GetNote(*m_NoteStack.begin());
						EventVec.push_back(cur);
						dupenote=true; 
						notecount++;
					}

					cur.Message=m_Score[m_Position];
					//cur.Frequency=m_RootFreq*pow(2.0f,*m_NoteStack.begin()/(float)m_NotesPerOctave);									
					cur.Frequency=m_Tuna->GetNote(*m_NoteStack.begin());
					cur.SlideFrequency=0;
					cur.Pan=m_Pan;
					cur.ID=m_Midi;
					cur.NoteNum=m_NoteNum++;
					cur.TimeStamp=TimeStamp;
					buildingnote=true;
				}
			} 
			break;
			case '<' :
			{ 
				if (m_Playing)
				{
					Event e;
					e.Frequency=0;
					e.Message='<';
					e.TimeStamp=TimeStamp;
					EventVec.push_back(e);
				}
			}
			break;
			case '^' :
			{ 
				if (m_Playing)
				{				
					Event e;
					e.Frequency=0;
					e.Message='^';
					e.TimeStamp=TimeStamp;
					EventVec.push_back(e);
				}
			}
			break;
			case '%' :
			{
				m_NoteStack.clear();
				m_NoteStack.push_front(m_RootNote);
			}
			break;
			case '[' :
			{
				if (m_NoteStack.size()<MAX_NOTESTACK) 
				{
					m_NoteStack.push_front(*m_NoteStack.begin());	
				}
			}
			break;
			case ']' :
			{
				if (m_NoteStack.size()>1) 
				{
					m_NoteStack.pop_front();
				}
			}
			break;
			case '@' :
			{
				// wait for a reset
				m_Position=-2; // account for the ++ below
				finished=true; 
			}
			break;
			default: // see if we have a message (a capital letter)
			{
				/*if (m_Playing && m_Score[m_Position]>=65 && m_Score[m_Position]<91)
				{
					// pass it on...
					Event e;
					e.Frequency=0;
					e.Message=m_Score[m_Position];
					e.TimeStamp=TimeStamp;
					EventVec.push_back(e);
				}*/
			}	
			break;
		}

		m_Position++;

		if (m_Position>=(int)m_Score.size()) 
		{
			if(abort) finished=true;
			abort=true; // stop us going round forever
			Reset(); 
		}
	}
}

void Pattern::Modify(const string &Name, float32 Value)
{
	if (Name=="Midi") m_Midi=(int)Value;
	else if (Name=="PanDistance") m_PanDistance=Value;
	else if (Name=="RootNote") 
	{
		if (Value>1) m_RootNote=(int)Value;
	}
	else if (Name=="Beat") m_Beat=Value;
	else if (Name=="BeatMult") m_BeatMult=Value;
	else if (Name=="Playing") m_Playing=(bool)Value;
}

void Pattern::Modify(const string &Name, const string &Value)
{
	if (Name=="Score") 
	{
		m_Score=Value;
		m_NewScore=Value;		
	}
	
	if (Name=="SafeScore") 
	{
		m_NewScore=Value;
		if (m_Score=="") m_Score=Value;
	}
}

void Pattern::Reset()
{
	m_Position=0;
	m_NoteNum=0;
	m_Score=m_NewScore;
}

int Pattern::SkipBranch(unsigned int Pos, string Str)
{
	bool found=false;
	int depth=0;
	while (!found)
	{
		char ch=Str[Pos];
		if (ch==']') 
		{
			depth--;
			if (depth==0) 
			{
				return Pos;
			}
		}
		if (ch=='[')
		{
			depth++;
		}
		Pos++;
		
		// fallen off the end...
		if (Pos>=Str.size()) found=true;
	}

	return Pos;
}
