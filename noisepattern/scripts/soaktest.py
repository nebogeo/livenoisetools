#!/usr/local/bin/python

import osc
import os
import sys
from random import *
from time import *

score_language = ['o','+','-','+','-','+','-','.','+','-','.','\\','|','/','<','1','2','3','4','5','6','7','8','9']
oscport = 88000

def MakeString(size):
	string=""
	for i in range(0,size):
		string+=choice(score_language)
	return string
		
def randomize(id):
	osc.Message("/np/modify",[id,"Midi",float(randint(1,10))]).sendlocal(oscport)
	osc.Message("/np/modify",[id,"Interval",float(0.1+random())]).sendlocal(oscport)
	osc.Message("/np/modify",[id,"PanDistance",float(random())]).sendlocal(oscport)
	osc.Message("/np/modify",[id,"RootNote",float(random()*10)]).sendlocal(oscport)
	osc.Message("/np/modify",[id,"NotesPerOctave",float(random()*50)]).sendlocal(oscport)
	#osc.Message("/np/notequantise",[randint(0,1)]).sendlocal(oscport)
	#osc.Message("/np/modify",[id,"Playing",randint(0,1)]).sendlocal(oscport)
	score = MakeString(randint(0,500))
	osc.Message("/np/modifystr",[id,"Score",score]).sendlocal(oscport)
	sleep(0.1)


if not os.fork():
	os.popen("noisepatternserver "+str(oscport))
	sys.exit()

sleep(1); # wait for server to come up

osc.Message("/np/add_dest_address",["88001"]).sendlocal(oscport)
osc.Message("/np/add_dest_address",["88002"]).sendlocal(oscport)
osc.Message("/np/add_dest_url",["/*/play"]).sendlocal(oscport)
osc.Message("/np/loadtuning",["/home/nebogeo/noiz/scales/scl/iran_diat.scl"]).sendlocal(oscport)
osc.Message("/np/settimebuffer",[float(0.5)]).sendlocal(oscport)
osc.Message("/np/settimelatency",[float(0.5)]).sendlocal(oscport)
#osc.Message("/np/notedebug",[1]).sendlocal(oscport)

# make a hundred patterns
for i in range(1,10):
	osc.Message("/np/new",[i]).sendlocal(oscport)
	randomize(i)
	sleep(0.1)	

while 1:
	for i in range(1,10):
		randomize(i)

