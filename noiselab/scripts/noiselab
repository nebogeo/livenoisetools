#!/usr/bin/env python

# Copyright (C) 2004 David Griffiths <dave@pawfal.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

from Tkinter import *
import os
import tkMessageBox
import tkFileDialog
import tkFont
import string
import random
import osc
import time
import mi

oscport = 4002

class sampleidstore:
	def __init__(self):
		self.nextsampleid = 0	
	def next(self):
		self.nextsampleid=self.nextsampleid+1
		return self.nextsampleid

sampleids = sampleidstore()

###########################################################################

# set global apperance options
mi.default_font = ("Courier",10,"bold")
mi.default_relief = FLAT
mi.col_bg2     = "#910000"
mi.col_active  = "#bfda00"
mi.col_bg	   = "#000000"
mi.default_patternwidth = 400
mi.default_w = 400
mi.default_h = 200

configpath = os.environ["HOME"]+"/.noiselab/"
samplespath = os.environ["NOISELAB_SAMPLE_LOCATION"]
	
##############################################################################

class Sample:
	def __init__(self,sample,name,path,group,parent):
		self.sample=sample
		self.volume=1.0
		self.speed=1.0
		self.pan=0.0
		self.poly=1.0
		self.starttime=0.0
		self.name=name
		self.path=path
		self.parent=parent
		self.group=group
		
	def Play(self):
		osc.Message("/play",[0,0,0,440.0,float(0),float(self.volume),float(0),0,""]).sendlocal(oscport)

	def Edit(self):
		editwin=Toplevel(bg=mi.col_bg)
		editwin.title(self.name)
		frame = Frame(editwin)
		frame.pack()
		self.volwidget = mi.Scale(frame, "Volume", 0.01, 0, 5, self.volcallback)
		self.volwidget.set(self.volume)
		self.volwidget.widget.pack()
		self.speedwidget = mi.Scale(frame, "Pitch", 0.01, 0, 10, self.speedcallback)
		self.speedwidget.set(self.speed)
		self.speedwidget.widget.pack()
		self.panwidget = mi.Scale(frame, "Pan", 0.01, -1, 1, self.pancallback)
		self.panwidget.set(self.pan)
		self.panwidget.widget.pack()
		self.polywidget = mi.Scale(frame, "Poly", 1, 0, 1, self.polycallback)
		self.polywidget.set(self.poly)
		self.polywidget.widget.pack()
		self.starttimewidget = mi.Scale(frame, "Start", 0.001, 0, 5, self.starttimecallback)
		self.starttimewidget.set(self.starttime)
		self.starttimewidget.widget.pack()

	def Delete(self):
		osc.Message("/unload",[self.sample]).sendlocal(oscport)
		self.parent.RemoveSample(self.sample)
		
	def MidiChannel(self,args):
		osc.Message("/map",[int(self.sample),int(self.midientry.get())]).sendlocal(oscport)
		
	def volcallback(self, value):
		self.volume=value
		osc.Message("/setglobals",[int(self.sample),float(self.volume),float(self.speed),float(self.pan)]).sendlocal(oscport)
		
	def speedcallback(self, value):
		self.speed=float(value)*440.0
		osc.Message("/setglobals",[int(self.sample),float(self.volume),float(self.speed),float(self.pan)]).sendlocal(oscport)

	def pancallback(self, value):
		self.pan=value
		osc.Message("/setglobals",[int(self.sample),float(self.volume),float(self.speed),float(self.pan)]).sendlocal(oscport)

	def polycallback(self, value):
		self.poly=value
		osc.Message("/setinfo",[int(self.sample),int(self.poly),float(self.starttime)]).sendlocal(oscport)
	
	def starttimecallback(self, value):
		self.starttime=value
		osc.Message("/setinfo",[int(self.sample),int(self.poly),float(self.starttime)]).sendlocal(oscport)

###########################################################################

class SampleGroup:
	def __init__(self,master):
		self.mapping=1
		self.sampleframe=Frame(master, bg=mi.col_bg, border=5)
		self.sampleframe.pack()
		self.mappingentry = mi.Entry(self.sampleframe)
		self.mappingentry.insert(0,"1")
		self.mappingentry.bind("<Return>",self.ChangeMapping)
		self.mappingentry.pack(side=LEFT)
		self.samples=[]

	def SetMapping(self,mapping):
		self.mapping=mapping
		self.mappingentry.delete(0,"end")
		self.mappingentry.insert(0,str(mapping))
		
	def ChangeMapping(self,args):
		self.mapping = int(self.mappingentry.get())
		for sample in self.samples:		
			osc.Message("/unmap",[int(sample.sample)]).sendlocal(oscport)
			osc.Message("/map",[int(sample.sample),self.mapping]).sendlocal(oscport)

###########################################################################
class Apperatus:
	def __init__(self,master):
		self.master=master
		self.patchname="mypatch"
		self.topframe=Frame(master, bg=mi.col_bg)
		self.topframe.pack()
		buttonframe=Frame(self.topframe, bg=mi.col_bg)
		buttonframe.pack()
		self.groupframe=Frame(buttonframe, bg=mi.col_bg2)
		self.groupframe.pack(side=LEFT)
		self.samples = []
		self.groups = {}		
		self.nextgroupid = 0
		
		menu = Menu(root, font=mi.default_font, bg=mi.col_bg2, relief=mi.default_relief, activebackground=mi.col_active)
		root.config(menu=menu)

		globalmenu = Menu(menu, font=mi.default_font, bg=mi.col_bg2, relief=mi.default_relief, activebackground=mi.col_active)
		menu.add_cascade(label="global", menu=globalmenu)
		globalmenu.add_command(label = "restart server", command=self.Restart)

		samplemenu = Menu(menu, font=mi.default_font, bg=mi.col_bg2, relief=mi.default_relief, activebackground=mi.col_active)
		menu.add_cascade(label="sample", menu=samplemenu)
		samplemenu.add_command(label = "load sample", command=self.LoadSample)
		samplemenu.add_command(label = "load all in dir", command=self.LoadAll)
		samplemenu.add_command(label = "load random from dir", command=self.LoadRandom)
		samplemenu.add_command(label = "load random recursive", command=self.LoadRandomRecursive)

		self.groupmenu = Menu(menu, font=mi.default_font, bg=mi.col_bg2, relief=mi.default_relief, activebackground=mi.col_active)
		menu.add_cascade(label="group", menu=self.groupmenu)
		self.groupmenu.add_command(label = "new group", command=self.AddGroup)
					
		self.status=Label(self.topframe, text="", bd=1, relief=mi.default_relief, width=35, anchor=W, font=mi.default_font, bg=mi.col_bg2)
		self.status.pack(side=BOTTOM,fill=X)
		
		botframe = Frame(self.topframe, bg=mi.col_bg)
		botframe.pack()		
					
		unmap = mi.Button(botframe, "Unmap All", self.UnMapAll)			
		unmap.pack(side=LEFT)
		unmap = mi.Button(botframe, "Set Clock", self.SetClock)			
		unmap.pack(side=LEFT)
		
		saveframe = Frame(self.topframe, bg=mi.col_bg)
		saveframe.pack()		
		
		self.save = mi.VButton(saveframe, "save patch", self.Save)
		self.save.grid(column=3, row=0)
		
		self.patchnamew = mi.BigEntry(saveframe)
		self.patchnamew.insert(0,"mypatch")
		self.patchnamew.bind("<Leave>",self.SetPatchName)
		self.patchnamew.grid(column=2, row=0)

		af = Frame(self.topframe, bg=mi.col_bg)
		af.pack()
		v = mi.Scale(af, "Volume", 0.001, 0, 5, self.SetVolume)	
		v.set(0.1)
		v.widget.grid(column=0, row=0)

		v = mi.Scale(af, "Pitch", 0.001, 0, 2, self.SetPitch)	
		v.set(1.0)
		v.widget.grid(column=1, row=0)
		
		self.patches = mi.Menubutton(saveframe, "patches")
		self.patches.grid(column=1, row=0)
		self.patches.menu= mi.Menu(self.patches)
		self.patches["menu"] = self.patches.menu

		self.UpdatePatchMenu()
		
		self.AddGroup()
		
		#print ("loading samples from "+samplespath+" ...")
		#filelist = os.listdir(samplespath)
		#for file in filelist:
		#	if os.path.splitext(file)[1].lower()==".wav":
		#		ID = noiselab.AddToQueue(str(samplespath+"/"+file))
		#		if ID:
		#			self.MakeSample(file,ID)
		#noiselab.LoadQueue()
						
	def Clear(self):
		for s in self.samples:
			s.frame.pack_forget()	
		self.samples=[]
		groupkeys = self.groups.keys()
		for k in groupkeys:
			self.groups[k].sampleframe.pack_forget()	
		self.groups = {}	
		osc.Message("/unmapall").sendlocal(oscport)
		osc.Message("/unloadall").sendlocal(oscport)
	
	def AddGroup(self):	
		self.groups[self.nextgroupid]=SampleGroup(self.groupframe)
		self.currentgroup=self.nextgroupid
		self.nextgroupid+=1
	
	def SetVolume(self, volume):
		osc.Message("/globalvolume",[float(volume)]).sendlocal(oscport)

	def SetPitch(self, pitch):
		osc.Message("/globalpitch",[float(pitch)]).sendlocal(oscport)
		
	def SetPatchName(self, args):
		self.patchname = str(self.patchnamew.get())
	
	def MakeSample(self, filename, ID):
		# get the leaf filename
		l = string.split(filename,"/")
		leafname=l[-1];
		newsample = Sample(ID,leafname,filename,self.currentgroup,self)
		frame = Frame(self.groups[self.currentgroup].sampleframe, bg=mi.col_bg)
		frame.pack(side=TOP)
		button = mi.VButton(frame, "X", newsample.Delete)
		button.pack(side=LEFT)
		newsample.frame=frame
		button = mi.WButton(frame, leafname, newsample.Play)
		button.pack(side=LEFT)
		button = mi.VButton(frame, "Edit", newsample.Edit)
		button.pack(side=RIGHT)
		self.samples.append(newsample)
		self.status.config(text="Loaded "+leafname)
		osc.Message("/map",[ID,self.groups[self.currentgroup].mapping]).sendlocal(oscport)
		self.groups[self.currentgroup].samples.append(newsample)
	
	def RemoveSample(self,sample):
		for s in self.samples:
			if s.sample==sample:
				s.frame.pack_forget()	
				self.samples.remove(s)
				self.groups[s.group].samples.remove(s)		
		
	def Start(self):
		osc.Message("/start",["noiselab"]).sendlocal(oscport)
		self.status.config(text="Started audio engine")	
		
	def Stop(self):
		osc.Message("/stop").sendlocal(oscport)
		self.status.config(text="Stopped audio engine")
		
	def LoadSample(self):
		filename = tkFileDialog.askopenfilename(initialdir=samplespath)
		if filename!="":
			ID = sampleids.next()
			osc.Message("/addtoqueue",[ID,filename]).sendlocal(oscport)
			osc.Message("/loadqueue").sendlocal(oscport)
			self.MakeSample(filename,ID)
		
	def LoadAll(self):
		directory = tkFileDialog.askopenfilename(title="choose one file in dir to load all from", initialdir=samplespath)
		directory = os.path.dirname(directory) # strip off filename
		filelist = os.listdir(directory)
		for file in filelist:
			if os.path.splitext(file)[1].lower()==".wav":
				ID = sampleids.next()
				osc.Message("/addtoqueue",[ID,str(directory+"/"+file)]).sendlocal(oscport)
				self.MakeSample(directory+"/"+file,ID)
		osc.Message("/loadqueue").sendlocal(oscport)
	
	def LoadRandom(self):
		directory = tkFileDialog.askopenfilename(title="choose one file in dir to load random from", initialdir=samplespath)
		directory = os.path.dirname(directory) # strip off filename
		filelist = os.listdir(directory)
		count = 0
		while count<9 :
			file = filelist[int(random.random()*len(filelist))]
			if os.path.splitext(file)[1].lower()==".wav":
				ID = sampleids.next()
				osc.Message("/addtoqueue",[ID,str(directory+"/"+file)]).sendlocal(oscport)
				self.MakeSample(directory+"/"+file,ID)
			count=count+1
		osc.Message("/loadqueue").sendlocal(oscport)

	def LoadRandomRecursive(self):
		directory = tkFileDialog.askopenfilename(title="choose one file in dir to load random from", initialdir=samplespath)
		directory = os.path.dirname(directory) # strip off filename
		os.path.walk(directory, LoadRandomRecursiveVisitor, self)
		osc.Message("/loadqueue").sendlocal(oscport)

	def UnMapAll(self):
		osc.Message("/unmapall").sendlocal(oscport)
		#for sample in self.samples
		#	sample.mididevice

	def SetClock(self):
		osc.Message("/setclock").sendlocal(oscport)
		
	def Save(self):
		file = open(configpath+"/recall/"+self.patchname,"w")
		groupkeys=self.groups.keys()
		for k in groupkeys:
			file.write("group\n")
			file.write(str(self.groups[k].mapping)+"\n")
			for s in self.groups[k].samples:
				file.write(s.path+"\n")	
	
	def Load(self,patchname):
		self.Clear()
		file = open(configpath+"/recall/"+patchname,"r")
		filename="dummy"
		firsttime=1
		while filename!="":
			filename=file.readline().strip()
			if filename!="":
				if filename=="group":
					self.AddGroup()
					self.groups[self.currentgroup].SetMapping(int(file.readline().strip()))
				else:	 
					if firsttime==1:
						self.AddGroup()
					ID = sampleids.next()
					osc.Message("/addtoqueue",[ID,filename]).sendlocal(oscport)
					self.MakeSample(filename,ID)
				firsttime=0
		osc.Message("/loadqueue").sendlocal(oscport)
	
	def UpdatePatchMenu(self):
		self.patchcount=0
		# get the patches
		patchlist = os.listdir(configpath+"/recall/")
		for patch in patchlist:
			self.patchcount=self.patchcount+1
			factory = PatchFactory(patch,self)
			self.patches.menu.add_command(label = patch, command=factory.Load)
	
	def Restart(self):
		self.Clear()
		os.popen("killall -9 noiselabserver")
		startserver()
		
def LoadRandomRecursiveVisitor(self,directory,filelist):	
	count = 0
	while count<2 :
		file = filelist[int(random.random()*len(filelist))]
		if os.path.splitext(file)[1].lower()==".wav":
			ID = sampleids.next()
			osc.Message("/addtoqueue",[ID,str(directory+"/"+file)]).sendlocal(oscport)
			self.MakeSample(directory+"/"+file,ID)
		count=count+1

class PatchFactory:
	def __init__(self,patch,instr):	
		self.patch=patch
		self.instr=instr
	
	def Load(self):	
		self.instr.Load(self.patch)

##############################################################################

def exitcallback():
	if tkMessageBox.askokcancel("Quit", "Do you really wish to quit?"):
		root.destroy()
		os.popen("killall -9 noiselabserver")

def startserver():
	if not os.fork():
		os.popen("noiselabserver "+str(oscport))
		sys.exit()

	time.sleep(1); # wait for server to come up
	osc.Message("/setclock").sendlocal(oscport)

startserver()

root = Tk()
root.protocol("WM_DELETE_WINDOW", exitcallback)
root.title("noiselab")
app = Apperatus(root)
root.mainloop()
