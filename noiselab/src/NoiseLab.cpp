// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "NoiseLab.h"
#include <spiralcore/JackClient.h>
//#include <spiralcore/SearchPaths.h>

static const int MAX_BUFFER=4096;

NoiseLab::NoiseLab(OSCServer *server, unsigned int SampleRate) :
m_Sampler(SampleRate),
m_SampleRate(SampleRate),
m_Running(false),
m_InitClock(true),
m_Server(server)
{
	//Sample::SetAllocator(new RealtimeAllocator(1024*1024*20));
	
	NewOutputBuffer();	
	
	JackClient* Jack=JackClient::Get();
	Jack->SetCallback(Run,this);
	Jack->Attach("noiselab");	
	if (Jack->IsAttached())
	{	
		int Left = Jack->AddOutputPort();
		m_JackPort.push_back(Left);
		Jack->SetOutputBuf(Left, m_BufferVec[0]->Left.GetNonConstBuffer());
 	    Jack->ConnectOutput(Left,"alsa_pcm:playback_1");
 	    int Right = Jack->AddOutputPort();
		m_JackPort.push_back(Right);
		Jack->SetOutputBuf(Right, m_BufferVec[0]->Right.GetNonConstBuffer());
 	    Jack->ConnectOutput(Right,"alsa_pcm:playback_2");
 		m_Running=true;
 	}
	
}

NoiseLab::~NoiseLab()
{
	cerr<<"deleting "<<this<<endl;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Interface thread side
// These functions must not touch any data other than m_CH in order to be threadsafe

void NoiseLab::NewOutputBuffer()
{		
	OutBuffer* NewBuffer = new OutBuffer;
	NewBuffer->Left.Allocate(MAX_BUFFER);
	NewBuffer->Right.Allocate(MAX_BUFFER);
	NewBuffer->Left.Zero();
	NewBuffer->Right.Zero();
  	m_BufferVec.push_back(NewBuffer);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Audio thread side

void NoiseLab::Run(void *RunContext, uint32 BufSize)
{
	NoiseLab *Noise=static_cast<NoiseLab*>(RunContext);	
	Noise->ProcessCommands();
	Noise->Process(BufSize);
}

void NoiseLab::ProcessCommands()
{	
	CommandRingBuffer::Command cmd;
	
	
	while (m_Server->Get(cmd))
	{
		string name = cmd.Name;
		if (name=="/stop")
		{
			exit(0);
			//Jack->Detach();
		}
		else if (name=="/upload")
		{
			//Sample temp=*(Sample*)m_Args.SamplePtr;
			//m_Args.Return = m_Sampler.Upload(&temp);
		}
		else if (name=="/addtoqueue")
		{
			m_Sampler.AddToQueue(cmd.GetInt(0), cmd.GetString(1));
		}
		else if (name=="/loadqueue")
		{
			m_Sampler.LoadQueue();
		}
		else if (name=="/unload")
		{
			m_Sampler.Unload(cmd.GetInt(0));
		}
		else if (name=="/setclock")
		{
			Time Now;
			Now.SetToNow();
			m_CurrentTime.Seconds=Now.Seconds;
			m_CurrentTime.Fraction=Now.Fraction;		
		}
		else if (name=="/play")
		{
			Event e;
			e.TimeStamp.Seconds=(unsigned int)cmd.GetInt(0);
			e.TimeStamp.Fraction=(unsigned int)cmd.GetInt(1);
			int device=cmd.GetInt(2);
			e.Frequency=cmd.GetFloat(3);
			e.SlideFrequency=cmd.GetFloat(4);
			e.Volume=cmd.GetFloat(5);
			e.Pan=cmd.GetFloat(6);
			e.Message=(char)cmd.GetInt(7);
			e.Channel=0;

			// if we need to read this device
			if (m_Map[device].Size>0)
			{
				// if it's an ordinary melodic voice
				if (m_Map[device].Size==1) e.ID = m_Map[device].SampleVec[0]; 
				else // percussion note
				{
					int sample=((int)e.Frequency)%m_Map[device].Size;
					e.ID = m_Map[device].SampleVec[sample]; 
					e.Frequency=441.0f;
				}

				if (e.TimeStamp.IsEmpty())
				{ 
					// play now
					m_Sampler.Play(0,e);
				}
				else
				{
					if (e.TimeStamp>m_CurrentTime) 
					{ 
						m_EventQueue.Add(e);
						//Trace(GREEN,RED,"%f",e.TimeStamp.GetDifference(m_CurrentTime));
						if (e.TimeStamp.GetDifference(m_CurrentTime)>5)
						{
							Trace(RED,YELLOW,"Reset clock? Event far in future %f seconds",e.TimeStamp.GetDifference(m_CurrentTime));
						} 	
					}
					else 
					{
						Trace(RED,YELLOW,"Event arrived too late, dumping");
					}
				}
			}
		}
		else if (name=="/setglobals")
		{
			Event e;
			e.Volume=cmd.GetFloat(1);
			e.Frequency=cmd.GetFloat(2);
			e.Pan=cmd.GetFloat(3);
			m_Sampler.Globals(cmd.GetInt(0),e);
		}
		else if (name=="/setinfo")
		{
			Sampler::SampleInfo i;
			i.Poly=cmd.GetInt(1);
			i.StartTime=cmd.GetFloat(2);
			m_Sampler.Info(cmd.GetInt(0),i);
		}
		else if (name=="/clear")
		{
			m_Sampler.UnloadAll();
			for (int device=0; device<NUM_DEVICES; device++)
			{
				m_Map[device].Size=0;
			}
		}
		else if (name=="/map")
		{
			// no mallocs allowed...
			Mapping *Mapping=&m_Map[cmd.GetInt(1)];
			Mapping->SampleVec[Mapping->Size]=cmd.GetInt(0);
			Mapping->Size++;
		}
		else if (name=="/unmap")
		{
			m_Map[cmd.GetInt(0)].Size=0;
		}
		else if (name=="/unmapall")
		{
			for (int device=0; device<NUM_DEVICES; device++)
			{
				m_Map[device].Size=0;
			}
		}
		else if (name=="/globalvolume")
		{
			m_Sampler.SetGlobalVolume(cmd.GetFloat(0));
		}
		else if (name=="/globalpitch")
		{
			m_Sampler.SetGlobalPitch(cmd.GetFloat(0));
		}
		else if (name=="/addsearchpath")
		{
          //SearchPaths::Get()->AddPath(cmd.GetString(0));
		}
		else
		{
			cerr<<"unknown command: "<<name<<endl;
		}
	
	}
}

void NoiseLab::Process(uint32 BufSize)
{
	for (vector<OutBuffer*>::iterator i = m_BufferVec.begin();
		i!=m_BufferVec.end(); i++)
	{
		(*i)->Left.Zero();
		(*i)->Right.Zero();
	}	
	
	Time NextTime = m_CurrentTime;
	NextTime.IncBySample(BufSize,m_SampleRate);

	Time LastTime = m_CurrentTime;
	m_CurrentTime.IncBySample(BufSize,m_SampleRate);
	
	Event e;
	while (m_EventQueue.Get(LastTime, m_CurrentTime, e))
	{	
		m_Sampler.Play(LastTime.GetDifference(e.TimeStamp),e);
	}
	
	if (m_BufferVec.size()>0)
	{
		if (BufSize>(unsigned int)(*m_BufferVec.begin())->Left.GetLength())
		{
			cerr<<"process buffer now "<<BufSize<<endl;
			int count=0;
			for (vector<OutBuffer*>::iterator i=m_BufferVec.begin(); i!=m_BufferVec.end(); i++)
			{ cerr<<m_JackPort[count]<<endl;
				(*i)->Left.Allocate(BufSize);
				JackClient::Get()->SetOutputBuf(m_JackPort[count], (*i)->Left.GetNonConstBuffer());
				count++; cerr<<m_JackPort[count]<<endl;
				(*i)->Right.Allocate(BufSize);
				JackClient::Get()->SetOutputBuf(m_JackPort[count], (*i)->Right.GetNonConstBuffer());
				count++;
			}
		}
		m_Sampler.Process(m_BufferVec, BufSize);
	}
	
	float cliplevel=0.1;
	
	for (vector<OutBuffer*>::iterator i = m_BufferVec.begin();
		i!=m_BufferVec.end(); i++)
	{
		for (unsigned int n=0; n<BufSize; n++)
		{
			if ((*i)->Left[n]<-cliplevel) (*i)->Left[n]=-cliplevel;
			if ((*i)->Left[n]>cliplevel) (*i)->Left[n]=cliplevel;
			if ((*i)->Right[n]<-cliplevel) (*i)->Right[n]=-cliplevel;
			if ((*i)->Right[n]>cliplevel) (*i)->Right[n]=cliplevel;
		}
	}	
	
	//m_CurrentTime=NextTime;
	//m_CurrentTime.Print();
}



