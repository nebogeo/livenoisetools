// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <spiralcore/Types.h>
#include <spiralcore/OSCServer.h>
#include <spiralcore/Time.h>
#include <spiralcore/EventQueue.h>
#include "Sampler.h"

#ifndef NOISE_ENGINE
#define NOISE_ENGINE

static const int MAX_SAMPLES_PER_DEVICE = 128;
static const int NUM_DEVICES = 64;

class NoiseLab 
{
public:
	NoiseLab(OSCServer *server, unsigned int Samplerate);
	virtual ~NoiseLab();

private:
		
	// audiothread side functions
	void NewOutputBuffer();
	static void Run(void *RunContext, uint32 BufSize);
    virtual void Process(uint32 BufSize);
	void ProcessCommands();
	
	Sampler m_Sampler;
	
	vector<OutBuffer*> m_BufferVec;
	vector<int> m_JackPort;
	unsigned long m_SampleRate;
	bool 	m_Running;
	Time	m_CurrentTime;
	bool	m_InitClock;
	
	class Mapping
	{
	public:
		Mapping() : Size(0) {}
		unsigned int Size;
		SampleID SampleVec[MAX_SAMPLES_PER_DEVICE];
	};
	 
	EventQueue m_EventQueue;
	OSCServer *m_Server;
	
	// no mallocs allowed...
	Mapping m_Map[NUM_DEVICES];
};

#endif //  NOISE_ENGINE
