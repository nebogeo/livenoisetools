// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <stdio.h>
#include <algorithm>
#include "Sampler.h"
#include <spiralcore/AsyncSampleLoader.h>

static const unsigned int SAFETY_MAX_CHANNELS=30;

Sampler::Sampler(unsigned int samplerate) :
m_PitchSnap(false),
m_Scale(0xffffffff),
m_LatencyCompensate(0),
m_SampleRate(samplerate),
m_GlobalVolume(1),
m_GlobalPitch(1),
m_NextSampleID(1),
m_NextEventID(1)
{
}

Sampler::~Sampler()
{
}

SampleID Sampler::Upload(Sample *s)
{
	return 0;
}

void Sampler::AddToQueue(SampleID ID, const string &Filename)
{
	if (m_SampleMap.find(ID)!=m_SampleMap.end()) return;
	
	Event Globals;
	m_GlobalsMap[ID]=Globals;
	m_SampleMap[ID]=AsyncSampleLoader::Get()->AddToQueue(Filename);
}

void Sampler::LoadQueue()
{
	AsyncSampleLoader::Get()->LoadQueue();
}
	
void Sampler::Unload(SampleID ID)
{
	map<SampleID,Sample*>::iterator i = m_SampleMap.find(ID);
	if (i!=m_SampleMap.end())
	{
		m_SampleMap.erase(i);
		map<SampleID,Event>::iterator g = m_GlobalsMap.find(ID);
		if (g!=m_GlobalsMap.end()) m_GlobalsMap.erase(g);
		map<SampleID,SampleInfo>::iterator n = m_InfoMap.find(ID);
		if (n!=m_InfoMap.end()) m_InfoMap.erase(n);
	}
	else
	{
		cerr<<"Could not find sample "<<ID<<" to unload"<<endl;
	}
}

void Sampler::UnloadAll()
{
	m_SampleMap.clear();
	m_GlobalsMap.clear();
	m_InfoMap.clear();
	m_NextSampleID=1;	
}	

EventID Sampler::Play(float timeoffset, const Event &Channel)
{
	map<SampleID,Sample*>::iterator i = m_SampleMap.find(Channel.ID);
	if (i!=m_SampleMap.end())
	{
		Event Copy = Channel;
		if (Copy.Frequency==0)
		{
			//cerr<<"Cancelling zero speed sample"<<endl;
			return 0;
		}
		
		// start playing from first non-zero sample
		if (0)//m_AutoCue)
		{
			float s=1;
			int c=0;
			while (s==0 && c<i->second->GetLength())
			{
				s=(*i->second)[c++];
			}
			Copy.Position=c;  
		}
		
		while (m_ChannelMap.size()>SAFETY_MAX_CHANNELS)
		{	
			Trace(RED,BLACK,"channels exceeded %d, culling!",SAFETY_MAX_CHANNELS);
			m_ChannelMap.erase(m_ChannelMap.begin());
		}

		SampleInfo *info=&m_InfoMap[Channel.ID];

		Copy.Position+=((info->StartTime+timeoffset)*(float)m_SampleRate)*(Copy.Frequency/440.0)*(m_GlobalsMap[Channel.ID].Frequency/440.0)*m_GlobalPitch;
		m_ChannelMap[m_NextEventID++]=Copy;
		
		// if poly mode is turned off, remove the last playing sample
		if (!info->Poly)
		{
			map<EventID,Event>::iterator i=m_ChannelMap.find(info->PlayingOn);
			while(i!=m_ChannelMap.end())
			{
	       		m_ChannelMap.erase(i);
				i=m_ChannelMap.find(info->PlayingOn);
			}
		}
		
		info->PlayingOn=m_NextEventID-1;
		return m_NextEventID-1;
	}
	else
	{
		cerr<<"Could not find sample "<<Channel.ID<<" to play"<<endl;
	}

	return 0;
}

void Sampler::Modify(EventID, const Event &Channel)
{
}

void Sampler::Globals(SampleID ID,const Event &Globals)
{
	m_GlobalsMap[ID]=Globals;
}

void Sampler::Info(SampleID ID,const SampleInfo &Info)
{
	m_InfoMap[ID]=Info;
}

void Sampler::LoadPitchMap(const float *Pitch, int Count)
{
	for (int n=0; n<Count; n++)
	{
		m_PitchVec.push_back(Pitch[n]);
	}
}

float Sampler::PitchSnap(float in)
{
	if (!m_PitchSnap || !m_PitchVec.size()) return in;
	
	// get the frequency from the speed
	float freq = in; // assumes 1.0 is middle A :)
	
	// find the closest note
	bool found=false;
	int start = 0;
	int end = m_PitchVec.size();
	int mid = 0;	
	while (!found)
	{
		mid = start+((end-start)/2);
		if (freq<m_PitchVec[mid]) end = mid;
		else start = mid;
		if (end-start == 1) found=true;
	}
		
	// knock out notes not in the current scale
	// todo: make this not 12 note/octave specific
	int NotesPerOctave = 12;
	
	if (m_Scale!=0xffffffff && m_Scale!=0)
	{
        // while the current note isn't in the scale
		while (!(m_Scale>>start%NotesPerOctave&1))
		{
			// just go up until we find a note availible
			start++;
		}
	}
	
	return m_PitchVec[start]/440.0f;
}

void Sampler::Process(vector<OutBuffer*> &BufferVec, uint32 BufSize)
{
	static uint32 highwater=0;
	if (m_ChannelMap.size()>highwater)
	{
		highwater=m_ChannelMap.size();
		//cerr<<"Channels highwater mark now at : "<<highwater<<endl;
	}

	map<EventID,Event>::iterator nexti=m_ChannelMap.begin();
	for (map<EventID,Event>::iterator i=m_ChannelMap.begin();
	       i!=m_ChannelMap.end();)
	{
		nexti++; // used so we can delete the current channel
		Event *ch = &i->second;
		map<SampleID,Sample*>::iterator sample = m_SampleMap.find(ch->ID);
		// check we still have the sample
		if (sample != m_SampleMap.end())
		{			
			Event *gl = &m_GlobalsMap[ch->ID];
			float Volume = ch->Volume*gl->Volume*m_GlobalVolume*0.1;
			float Speed =  (ch->Frequency/440.0)*(gl->Frequency/440.0)*m_GlobalPitch;
			
			float Pan = 0;
			
			if (gl->Pan!=0) Pan = (ch->Pan+gl->Pan)/2.0f; // average
			else Pan = ch->Pan; // just channel pan
				
			Pan = 0.5f+Pan/2.0f; // 0 -> 1
			float Left = Pan;		
			float Right = 1-Pan;
						
			for (uint32 n=0; n<BufSize; n++)
			{	
				if (ch->Position<sample->second->GetLength() && ch->Position>=0)
				{
					BufferVec[ch->Channel]->Left.Set(n,BufferVec[ch->Channel]->Left[(int)n]+(*sample->second)[ch->Position]*Volume*Left);
					BufferVec[ch->Channel]->Right.Set(n,BufferVec[ch->Channel]->Right[(int)n]+(*sample->second)[ch->Position]*Volume*Right);
				}
				
				ch->Position+=Speed;
							
				if (ch->Position>=sample->second->GetLength())
				{
					m_ChannelMap.erase(i);
					break;
				}
			}
		}
		else // sample deleted, so free the channel
		{
			m_ChannelMap.erase(i);
		}
		i=nexti;
	}
}

