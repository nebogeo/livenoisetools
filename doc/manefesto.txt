a manefesto of sorts...

instruments for playing in the old sense, not audio workstation applications for 
slavishly polishing songs. this software represents an emphasis on improvising 
music live, using a lot of different techniques. the goal is to have no need 
for loading or saving ideas to use later - the time is now! music you make 
should reflect your current feelings, expressing yourself should be more 
important than staying in tune. it's not that new an idea!

this is what this project is about, and the way I want to make music.

clearly an important factor here is to have an expressive and honest interface 
that is as open ended as possible. it doesn't achieve these goals yet, and 
probably never will, but this is an ongoing and experimental process, the 
following techniques and ideas are being used, and represent the current 
thinking:

live programming
we are making computer music here, so interact with the computer with the least 
possible stuff in the way. (if you want to play non-computer music, go buy a 
guitar, it's a lot better at the job) live programming is being explored by a 
number of people who call themselves toplap (http://www.toplap.org) and means 
programming itself becoming a valid and engaging part of a performance. 

genetic programming
genetic programming is almost the complete opposite and complimentary approach 
of live programming. it allows players to make purely aesthetic decisions based 
on simple choices (ie you can do it drunk, this could be important). it's been 
likened to art by gardening. genetic programming allows large areas of 
complexity to be rapidly explored, or structures to be fine tuned. by allowing 
live and genetic programming to be swapped freely by the player, they can hack 
some resulting code manually to suit and then evolve it further, ad infinitum.

artificial life
both these techniques are used to drive artificial life based algorithms for 
creating music scores or synthesis networks for making the music. the advantage 
of using artificial life in this way is it's inherent emergent complexity. it 
means complex melodies and structures can be created quickly and with a 
surprising amount of control.

physical interfaces
the main problem with making music with a computer is that it hardly inspires 
you to make interesting work. a mouse and keyboard are rubbish for expression 
(IMHO). building alternative interfaces is a vital part of this project. the 
physical interfaces themselves should also follow these goals of expressivity, 
and open endedness.

a lot of these tools do not closely follow all these guidelines - either for 
pragmatic reasons, or because they were developed while these ideals were being 
formed. whatever, these ideas should not be seen as hard and fast rules... 
the music is the important thing.
