// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "Instrument.h"
#include "MoogVoice.h"
#include "AdditiveVoice.h"
#include "DirtyVoice.h"
#include "PhaseMod.h"
#include "DrumVoice.h"

static const unsigned int MAX_POLY=5;

Instrument::Instrument(const string &Type, int SampleRate,int Poly,int Device) :
m_Device(Device),
m_NextVoice(0),
m_Type(Type),
m_Poly(Poly),
m_SampleRate(SampleRate),
m_Pan(0.5f),
m_Volume(1.0f),
m_Distortion(0),
m_HardClip(0),
m_CrushFreq(0),
m_CrushBits(0),
m_Delay(SampleRate)
{
	Build();
}

Instrument::~Instrument() 
{
	for (vector<Voice*>::iterator i=m_VoiceVec.begin(); i!=m_VoiceVec.end(); i++)
	{
		delete *i;
	}
}

void Instrument::Build()
{
	m_VoiceVec.clear();
	for (unsigned int n=0; n<MAX_POLY; n++)
	{
		if (m_Type=="sub") m_VoiceVec.push_back(new MoogVoice(m_SampleRate));
		else if (m_Type=="add") m_VoiceVec.push_back(new AdditiveVoice(m_SampleRate));
		else if (m_Type=="dirt") m_VoiceVec.push_back(new DirtyVoice(m_SampleRate));
		else if (m_Type=="fm") m_VoiceVec.push_back(new PhaseModVoice(m_SampleRate));
		else if (m_Type=="drum") m_VoiceVec.push_back(new DrumVoice(m_SampleRate));
	}
}

void Instrument::Modify(const string &Name, float Value)
{
	if (Name=="map")
	{ 
		m_Device=(int)Value;
	}
	else if (Name=="poly")
	{
		m_Poly=(int)Value;
		if (m_Poly>=MAX_POLY) m_Poly=MAX_POLY-1;
		if (m_Poly<=0) m_Poly=1;
	}
	else if (Name=="mainvolume") m_Volume=Value;
	else if (Name=="pan") m_Pan=Value;
	else if (Name=="crushfreq") m_CrushFreq=Value;
	else if (Name=="crushbits") m_CrushBits=Value;
	else if (Name=="distort") m_Distortion=Value;
	else if (Name=="delayfb") m_Delay.SetFeedback(Value);
	else if (Name=="delay") m_Delay.SetDelay(Value);
	else
	{
		for (unsigned int i=0; i<MAX_POLY; i++)
		{
			m_VoiceVec[i]->Modify(Name,Value);
		}
	}
}

void Instrument::Trigger(const TriggerInfo &t)
{
	m_VoiceVec[m_NextVoice++]->Trigger(t);
	if (m_NextVoice>=m_Poly) m_NextVoice=0;
}
	
void Instrument::Process(unsigned int BufSize, Sample &left, Sample &right)
{		
	if (BufSize>(unsigned int)m_InstL.GetLength())
	{
		m_InstL.Allocate(BufSize);
		m_InstR.Allocate(BufSize);
		m_PolyL.Allocate(BufSize);
		m_PolyR.Allocate(BufSize);
	}
	
	m_InstL.Zero();
	m_InstR.Zero();
	
	for (unsigned int i=0; i<m_Poly; i++)
	{
		m_VoiceVec[i]->Process(BufSize,m_PolyL,m_PolyR);
		m_InstL.Mix(m_PolyL);
		m_InstR.Mix(m_PolyR);
	} 
	
	m_Delay.Process(BufSize, m_InstL, m_InstL);
	m_Delay.Process(BufSize, m_InstR, m_InstR);
	
	if (m_Distortion>0) 
	{
		Distort(m_InstL,m_Distortion);
		Distort(m_InstR,m_Distortion);
	}
	
	if (m_HardClip>0) 
	{
		HardClip(m_InstL,1-m_HardClip);
		HardClip(m_InstR,1-m_HardClip);
	}
	
	if (m_CrushFreq>0 && m_CrushBits>0)
	{
		Crush(m_InstL,m_CrushFreq,m_CrushBits);
		Crush(m_InstR,m_CrushFreq,m_CrushBits);
	}
	
	float oneoverpoly=1/(float)m_Poly;	
	for (unsigned int n=0; n<BufSize; n++)
	{
		m_InstL[n]*=m_Volume*m_Pan*oneoverpoly;
		m_InstR[n]*=m_Volume*(1-m_Pan)*oneoverpoly;
	}
	
	left.Mix(m_InstL);
	right.Mix(m_InstR);
}
