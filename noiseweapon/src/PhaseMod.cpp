// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "PhaseMod.h"
#include <math.h>

using namespace std;
map<string,int> PhaseModVoice::m_Names;

PhaseModVoice::PhaseModVoice(int SampleRate) :
Voice(SampleRate),
m_WaveTable(SampleRate),
m_ModWaveTable(SampleRate),
m_ModEnvelope(SampleRate),
m_FBEnvelope(SampleRate),
m_Envelope(SampleRate),
m_FeedBack(MAX_BUFFER),
m_Buffer(MAX_BUFFER)
{
}

void PhaseModVoice::RegisterNames()
{
	RegisterName("type",m_Names);
	RegisterName("freq",m_Names);
	RegisterName("slide",m_Names);
	RegisterName("modtype",m_Names);
	RegisterName("modfreq",m_Names);
	RegisterName("modslide",m_Names);
	RegisterName("fbattack",m_Names);
	RegisterName("fbdecay",m_Names);
	RegisterName("fbsustain",m_Names); 
	RegisterName("fbrelease",m_Names); 
	RegisterName("fbvolume",m_Names);
	RegisterName("modattack",m_Names); 
	RegisterName("moddecay",m_Names);
	RegisterName("modsustain",m_Names);
	RegisterName("modrelease",m_Names);
	RegisterName("modvolume",m_Names);
	RegisterName("attack",m_Names);
	RegisterName("decay",m_Names);
	RegisterName("sustain",m_Names);
	RegisterName("release",m_Names);
	RegisterName("volume",m_Names);
}

void PhaseModVoice::Process(unsigned int BufSize, Sample &left, Sample &right)
{
	if (BufSize>(unsigned int)m_Buffer.GetLength())
	{
		m_FeedBack.Allocate(BufSize);
		m_Buffer.Allocate(BufSize);
	}

	m_FBEnvelope.Process(BufSize, m_FeedBack, m_Buffer);
	m_WaveTable.Process(BufSize, left);
	m_ModWaveTable.Process(BufSize, right);
	m_ModEnvelope.Process(BufSize, right, m_Buffer);
	
	for (unsigned int n=0; n<BufSize; n++)
	{
		left[n]=sin((left[n]+right[n]+m_FeedBack[n])*RAD);
	}
	
	m_Envelope.Process(BufSize, left,  m_Buffer);
	
	StereoMix(left,left,right);
	m_FeedBack=left;
}

void PhaseModVoice::Trigger(const TriggerInfo &t)
{
	Voice::Trigger(t);
	m_WaveTable.Trigger(t.Time,t.Pitch,t.SlidePitch,0.5);
	m_ModWaveTable.Trigger(t.Time,t.Pitch,t.SlidePitch,0.5);
	
	if (t.Accent) 
	{
		m_ModEnvelope.Trigger(t.Time,t.Pitch,t.Volume);
		m_FBEnvelope.Trigger(t.Time,t.Pitch,t.Volume);
	}
	
	m_Envelope.Trigger(t.Time,t.Pitch,t.Volume);
}

void PhaseModVoice::Modify(const string &name, float v)
{ 
	map<string,int>::iterator i=m_Names.find(name);
	if (i==m_Names.end()) return;
	
	switch (i->second)
	{
		case 1: m_WaveTable.SetType((int)v); break;
		case 2: m_WaveTable.SetFineFreq(v); break;
		case 3: m_WaveTable.SetSlideLength(v); break;	
		case 4: m_ModWaveTable.SetType((int)v); break;
		case 5: m_ModWaveTable.SetFineFreq(v); break;
		case 6: m_ModWaveTable.SetSlideLength(v); break;
		case 7: m_FBEnvelope.SetAttack(v); break;
		case 8: m_FBEnvelope.SetDecay(v); break;
		case 9: m_FBEnvelope.SetSustain(v); break;
		case 10: m_FBEnvelope.SetRelease(v); break;
		case 11: m_FBEnvelope.SetVolume(v); break;
		case 12: m_ModEnvelope.SetAttack(v); break;
		case 13: m_ModEnvelope.SetDecay(v); break;
		case 14: m_ModEnvelope.SetSustain(v); break;
		case 15: m_ModEnvelope.SetRelease(v); break;
		case 16: m_ModEnvelope.SetVolume(v); break;
		case 17: m_Envelope.SetAttack(v); break;
		case 18: m_Envelope.SetDecay(v); break;
		case 19: m_Envelope.SetSustain(v); break;
		case 20: m_Envelope.SetRelease(v); break;
		case 21: m_Envelope.SetVolume(v); break;
		default : cerr<<"unhandled modify code "<<i->second<<endl;
	}
}




