// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <iostream>
#include <string>
#include <map>
#include <spiralcore/Types.h>
#include <spiralcore/Sample.h>

#ifndef VOICE
#define VOICE

static const int MAX_BUFFER=0;
static const int DEF_POLY=1;
static const int SAMPLERATE=44100;

using namespace spiralcore;
using namespace std;

struct TriggerInfo
{
	float Time;
	float Pitch;
	float SlidePitch;
	float Volume;
	float Pan;
	bool  Accent;
};

class Voice
{
public:
	Voice(int SampleRate);
	virtual ~Voice() {}
	
	virtual void Process(unsigned int BufSize, Sample &left, Sample &right);
	virtual void Trigger(const TriggerInfo &t);
	virtual void Modify(const string &name, float v);
	
protected:
	
	void StereoMix(const Sample &src, const Sample &left, const Sample &right);
	static void RegisterName(const string &name, map<string,int> &names);

	float m_Volume;
	float m_Pitch;
	float m_Pan,m_LastPan;
	float m_Time;
	
	float m_Samplerate;
};

#endif
