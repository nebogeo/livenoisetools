// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "Voice.h"
#include "Modules.h"

#ifndef MOOG_VOICE
#define MOOG_VOICE

using namespace spiralcore;

class MoogVoice : public Voice
{
public:
	MoogVoice(int SampleRate);
	virtual ~MoogVoice() {}
	
	virtual void Process(unsigned int BufSize, Sample &left, Sample &right);
	virtual void Trigger(const TriggerInfo &t);
	virtual void Modify(const string &name, float v);

	static void RegisterNames();
	
private:
	static map<string,int> m_Names;

	WaveTable m_WaveTableA;
	Envelope  m_EnvelopeA;
	WaveTable m_WaveTableB;
	Envelope  m_EnvelopeB;
	FilterWrapper m_Filter;
	Envelope  m_EnvelopeF;	
	WaveTable m_LFO;
	
	int m_FilterType;
	float m_LFOLevel;
	float m_Ring;
	
	Sample m_FilterCV;
	Sample m_LFOCV;
	
	float m_Time;
};

#endif
