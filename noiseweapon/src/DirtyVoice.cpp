// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "DirtyVoice.h"

using namespace std;

DirtyVoice::DirtyVoice(int SampleRate) :
Voice(SampleRate),
m_WaveTableA(SampleRate),
m_WaveTableB(SampleRate),
m_Envelope(SampleRate),
m_CrushFreq(1),
m_CrushBits(32),
m_DistortAmount(0)
{
}

void DirtyVoice::Process(unsigned int BufSize, Sample &left, Sample &right)
{
	if (BufSize>(unsigned int)m_Buffer.GetLength())
	{
		cerr<<"process buffer now "<<BufSize<<" !!!"<<endl;
		m_Buffer.Allocate(BufSize);
	}

	m_WaveTableA.Process(BufSize, left);
	m_WaveTableB.Process(BufSize, right);
	left.Mix(right);
	m_Envelope.Process(BufSize, left,  m_Buffer);
	Crush(left,m_CrushFreq,m_CrushBits);
	Distort(left,m_DistortAmount);
	
	StereoMix(left,left,right);
}

void DirtyVoice::Trigger(const TriggerInfo &t)
{
	Voice::Trigger(t);
	m_WaveTableA.Trigger(t.Time,t.Pitch,t.SlidePitch,0.1);
	m_WaveTableB.Trigger(t.Time,t.Pitch+0.5,t.SlidePitch+0.5,0.1);
	m_Envelope.Trigger(t.Time,t.Pitch,t.Volume);
}

void DirtyVoice::Modify(const string &name, float v)
{ 
	if (name=="typea") m_WaveTableA.SetType((int)v);
	else if (name=="freqa") m_WaveTableA.SetFineFreq(v);
	else if (name=="slidea") m_WaveTableA.SetSlideLength(v);
	
	else if (name=="typeb") m_WaveTableB.SetType((int)v);
	else if (name=="freqb") m_WaveTableB.SetFineFreq(v);
	else if (name=="slideb") m_WaveTableB.SetSlideLength(v);

	else if (name=="attack") m_Envelope.SetAttack(v);
	else if (name=="decay") m_Envelope.SetDecay(v);
	else if (name=="sustain") m_Envelope.SetSustain(v);
	else if (name=="release") m_Envelope.SetRelease(v);
	else if (name=="volume") m_Envelope.SetVolume(v);

	else if (name=="crushfreq") m_CrushFreq=v;
	else if (name=="crushbits") m_CrushBits=v;
	else if (name=="distort") m_DistortAmount=v;
}




