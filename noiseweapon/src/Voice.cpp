// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "Voice.h"
#include <math.h>
#include <iostream>

using namespace std;

Voice::Voice(int SampleRate) : 
m_Volume(0), 
m_Pitch(0),
m_Pan(0.5),
m_LastPan(0.5),
m_Time(0)
{
	m_Samplerate=SampleRate;
}

void Voice::Trigger(const TriggerInfo &t)
{
	m_Volume=t.Volume;
	m_Pitch=t.Pitch;
	m_Pan=t.Pan;
	m_Time=0;
}

void Voice::Process(unsigned int BufSize, Sample &left, Sample &right)
{
	m_Time+=BufSize/m_Samplerate;
}

void Voice::Modify(const string &name, float v)
{
}


void Voice::StereoMix(const Sample &src, const Sample &left, const Sample &right)
{
	// a bit of a crap filter to smooth clicks
	static float SMOOTH = 0.999;
	static float ONEMINUS_SMOOTH = 1-SMOOTH;
	if (!feq(m_Pan,m_LastPan,0.01))
	{
		float rpan=0;
		float lpan=0;
		for (int i=0; i<src.GetLength(); i++)
		{
			m_LastPan=(m_Pan*ONEMINUS_SMOOTH+m_LastPan*SMOOTH);
			rpan=(1+m_LastPan)*0.5f;
			lpan=1-rpan;
			right[i]=src[i]*rpan;
			left[i]=src[i]*lpan;
		}
	}
	else // bit faster if we have not recently changed panning
	{
		float rpan=(1+m_LastPan)*0.5f;
		float lpan=1-rpan;
		for (int i=0; i<src.GetLength(); i++)
		{
			right[i]=src[i]*rpan;
			left[i]=src[i]*lpan;
		}
	}
}

void Voice::RegisterName(const string &name, map<string,int> &names)
{
	names[name]=names.size();
}

