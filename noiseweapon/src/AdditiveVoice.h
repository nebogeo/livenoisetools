// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "Voice.h"
#include "Modules.h"

#ifndef ADDITIVE_VOICE
#define ADDITIVE_VOICE

#define NUM_HARMS 8

using namespace spiralcore;

static const int SINCOS_TABLESIZE=2048;
static const float TWO_PI=3.141592654*2.0f;

class AdditiveVoice : public Voice
{
public:
	AdditiveVoice(int SampleRate);
	virtual ~AdditiveVoice();
	
	virtual void Process(unsigned int BufSize, Sample &left, Sample &right);
	virtual void Trigger(const TriggerInfo &t);
	virtual void Modify(const string &name, float v);
	
	float dSin(float a);

private:
	float m_Level[NUM_HARMS];
	SimpleWave *m_Wave[NUM_HARMS];
	
	Envelope  m_Envelope;
	Sample m_Buffer;
		
	float SinTab[SINCOS_TABLESIZE];
	float CosTab[SINCOS_TABLESIZE];
	const float SINCOS_LOOKUP;
};

#endif
