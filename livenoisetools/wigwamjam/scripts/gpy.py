# Copyright (C) 2006 David Griffiths <dave@pawfal.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

from types import *
import random
import math

class node_idgen:
	def __init__(self):
		self.id=0
	
	def next(self):
		self.id=self.id+1
		return self.id
		
node_id = node_idgen()

class basenode:
	def __init__(self):
		self.args=[]
		self.id = node_id.next()
		
	def clone(self):
		pass

class function(basenode):
	def __init__(self,name="",numargs=0,rettype=NoneType,argtypes=[]):
		basenode.__init__(self)
		self.nodetype="function"
		self.name=name
		self.numargs=numargs
		self.rettype=rettype
		self.argtypes=argtypes

	def clone(self):
		return self._clone(function(self.name,self.numargs,self.rettype,self.argtypes))
		
	def _clone(self,other): # recurse through arguments
		for i in self.args:
			other.args.append(i.clone())
		return other
		
class terminal(basenode):
	def __init__(self,low=0,high=1):
		basenode.__init__(self)
		self.nodetype="terminal"
		self.name="terminal"
		self.low=low
		self.high=high	
		self.value=low+(random.random()*(high-low))
		self.numargs=0
		self.rettype=FloatType
		self.argtypes=NoneType
		
	def clone(self):
		return terminal(self.low,self.high)
	
class tree:
	def __init__(self,function_table,depth):
		self.function_table=function_table
		self.root=function()
		self.randomise(depth)
		self.depth=depth # not exact...		

	def randomise(self,depth):
		self.root=self.function_table[random.randrange(0,len(self.function_table))].clone()
		self._randomise(self.root,depth)

	def _randomise(self,node,depth):
		for i in range(0,node.numargs):
			if depth>0:
				node.args.append(self.function_table[random.randrange(0,len(self.function_table))].clone())
				if node.args[i].nodetype=="function": 
					self._randomise(node.args[i],depth-1)
			else: # add a terminal as we have bottomed out
				node.args.append(self.function_table[0].clone())

	def mutate(self,strength):
		for i in range(0, strength):
			mutationtype = random.randrange(0,3)
		
			if mutationtype==0: 
				# remove subtree (set to default terminal)
				node = self.pick_random_node(random.randrange(0,self.depth))
				if len(node.args)>0:
					node.args[random.randrange(0,len(node.args))] = self.function_table[0].clone()

			# seems to cause malformed trees (functions with no args)
			#if mutationtype==1: 
			#	# add a new subtree
			#	depth = random.randrange(0,self.depth)
			#	node = self.pick_random_node(depth)
			#	if len(node.args)>0:
			#		self._randomise(node,self.depth-depth)		

			if mutationtype==2: 
				# dupe subtree - this also re-randomises the terminals at the moment
				# this also is able to duplicate a subtree within itself
				node = self.pick_random_node(random.randrange(0,self.depth))
				if len(node.args)>0:
					node.args[random.randrange(0,len(node.args))] = self.pick_random_node(random.randrange(0,self.depth)).clone()
		
	def pick_random_node(self,depth):
		return self._pick_random_node(self.root,depth)

	def _pick_random_node(self,node,depth):
		if depth==0 or len(node.args)==0:
			return node
		return self._pick_random_node(node.args[random.randrange(0,len(node.args))],depth-1)

	def code(self):
		return self._code(self.root)

	def _code(self,func,depth=0):
		if func.nodetype=="function":
			if len(func.args)>0:
				line = "("+func.name+" "
				for i in range(0,len(func.args)):
					line = line+self._code(func.args[i],depth+1)
					line = line+" "
				line = line+")"
			else:
				line = func.name
		else:
			line = str(func.value)
		
		return line
	
	def findfunc(self, name):
		for f in self.function_table:
			if f.name==name:
				return f.clone()
		return 0

	def _skpws(self, code):
		while code[self.pos]==" ":
			self.pos += 1
			
	def build(self,code):
		self.pos=0
		self.root = self._build(code)		
	
	def _build(self,code):
		func = 0
		self._skpws(code)
		if code[self.pos]=="(":
			# we have a function
			self.pos+=1 # skip (
			funcname=""
			
			while code[self.pos]!=" ":
				funcname+=code[self.pos]
				self.pos+=1
			
			func = self.findfunc(funcname)
			if func!=0:
				while self.pos<len(code) and code[self.pos]!=")":
					self._skpws(code)
					if code[self.pos]!=")":						
						func.args.append(self._build(code))
				self.pos += 1 # skip the ")"	
			else:
				print funcname+" not understood"

			
		else:
			# it's a terminal (variable or literal value)
			value=""
			while code[self.pos]!=" ":
				value+=code[self.pos]
				self.pos+=1

			func = self.findfunc(value)
			if func==0:
				func = self.function_table[0].clone()
				func.value=float(value)
					
		if func==0:
			print "error in loading... erm..."
		
		return func
		
