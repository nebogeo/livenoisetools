// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "Voice.h"
#include "Modules.h"
#include <vector>

#ifndef INSTRUMENT
#define INSTRUMENT

using namespace std;
using namespace spiralcore;

class Instrument
{
public:
	Instrument(const string &Type, int SampleRate, int Poly, int Device);
	~Instrument();
	
	void Modify(const string &Name, float Value);
	int GetDevice() { return m_Device; }
	void Process(unsigned int BufSize, Sample &left, Sample &right);
	void Trigger(const TriggerInfo &t);
	
private:
	void Build();

	vector<Voice*> m_VoiceVec;
	int m_Device;
	unsigned int m_NextVoice;
	string m_Type;
	unsigned int m_Poly;
	int m_SampleRate;
	float m_Pan;
	float m_Volume;
	float m_Distortion;
	float m_HardClip;
	float m_CrushFreq;
	float m_CrushBits;
	Delay m_Delay;

	Sample m_InstL;
	Sample m_InstR;
	Sample m_PolyL;
	Sample m_PolyR;
};

#endif
