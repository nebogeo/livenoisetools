// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "Voice.h"
#include "Modules.h"

#ifndef PhaseMod_VOICE
#define PhaseMod_VOICE

using namespace spiralcore;

class PhaseModVoice : public Voice
{
public:
	PhaseModVoice(int SampleRate);
	virtual ~PhaseModVoice() {}
	
	virtual void Process(unsigned int BufSize, Sample &left, Sample &right);
	virtual void Trigger(const TriggerInfo &t);
	virtual void Modify(const string &name, float v);
	
	static void RegisterNames();

private:
	static map<string,int> m_Names;

	WaveTable m_WaveTable;
	WaveTable m_ModWaveTable;
	Envelope  m_ModEnvelope;
	Envelope  m_FBEnvelope;
	Envelope  m_Envelope;
	
	Sample m_FeedBack;
	Sample m_Buffer;
		
	float m_Time;
};

#endif
