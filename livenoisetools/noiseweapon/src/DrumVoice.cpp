// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "DrumVoice.h"
#include <math.h>

using namespace std;

map<string,int> DrumVoice::m_Names;

DrumVoice::DrumVoice(int SampleRate) :
Voice(SampleRate),
m_KickFreqEnvelope(SampleRate),
m_KickEnvelope(SampleRate),
m_SnareWaveTable(SampleRate),
m_SnareEnvelope(SampleRate),
m_SnareFilter(SampleRate),
m_SnareFilterEnvelope(SampleRate),
m_Hat1WaveTable(SampleRate),
m_Hat1Envelope(SampleRate),
m_Hat1Filter(SampleRate),
m_Hat2WaveTable(SampleRate),
m_Hat2Envelope(SampleRate),
m_Hat2Filter(SampleRate),
m_FilterType(0),
m_KickFreq(1),
m_FilterCV(MAX_BUFFER),
m_LPF(MAX_BUFFER),
m_BPF(MAX_BUFFER),
m_HPF(MAX_BUFFER),
m_EnvCV(MAX_BUFFER),
m_KickBuf(MAX_BUFFER),
m_Hat1Buf(MAX_BUFFER),
m_Hat2Buf(MAX_BUFFER),
m_SnareBuf(MAX_BUFFER)
{
	m_Hat1WaveTable.SetType(WaveTable::NOISE);
	m_Hat2WaveTable.SetType(WaveTable::PINKNOISE);
	m_SnareWaveTable.SetType(WaveTable::PINKNOISE);
}

void DrumVoice::RegisterNames()
{
	RegisterName("kickfreqdecay",m_Names);
	RegisterName("kickfreqvolume",m_Names);
	RegisterName("kickfreq",m_Names); 
	RegisterName("kickdecay",m_Names); 
	RegisterName("kickvolume",m_Names); 
	RegisterName("hat1decay",m_Names);	
	RegisterName("hat1volume",m_Names);  
	RegisterName("hat1cutoff",m_Names);  
	RegisterName("hat1resonance",m_Names);  
	RegisterName("hat2decay",m_Names);	
	RegisterName("hat2volume",m_Names);  
	RegisterName("hat2cutoff",m_Names);  
	RegisterName("hat2resonance",m_Names);  
	RegisterName("snaredecay",m_Names);   
	RegisterName("snarevolume",m_Names);  
	RegisterName("snarefilterattack",m_Names); 
	RegisterName("snarefilterdecay",m_Names);  
	RegisterName("snarefiltersustain",m_Names);
	RegisterName("snarefilterrelease",m_Names);
	RegisterName("snarefiltervolume",m_Names); 
	RegisterName("snareftype",m_Names); 
	RegisterName("snarecutoff",m_Names); 
	RegisterName("snareresonance",m_Names);
}

void DrumVoice::Process(unsigned int BufSize, Sample &left, Sample &right)
{
	if (BufSize>(unsigned int)m_FilterCV.GetLength())
	{
		m_FilterCV.Allocate(BufSize);
		m_LPF.Allocate(BufSize);
		m_BPF.Allocate(BufSize);
		m_HPF.Allocate(BufSize);
		m_EnvCV.Allocate(BufSize);
		m_KickBuf.Allocate(BufSize);
		m_Hat1Buf.Allocate(BufSize);
		m_Hat2Buf.Allocate(BufSize);
		m_SnareBuf.Allocate(BufSize);
	}
	
	left.Zero();
	right.Zero();
	
	// kick
	m_KickFreqEnvelope.Process(BufSize, m_KickBuf, m_EnvCV);
	for (unsigned int n=0; n<BufSize; n++)
	{	
		m_KickBuf[n]=sin(m_KickTime);
		m_KickTime+=(m_EnvCV[n]*m_EnvCV[n]+m_KickFreq)*0.01;
		if (m_KickTime>6.2831853) m_KickTime-=6.2831853;
	}	

	m_KickEnvelope.Process(BufSize, m_KickBuf, m_EnvCV);
	
	// hihat 1
	m_Hat1WaveTable.Process(BufSize, m_Hat1Buf);
	m_Hat1Envelope.Process(BufSize, m_Hat1Buf, m_FilterCV);
	m_Hat1Filter.Process(BufSize, m_Hat1Buf, m_FilterCV, &m_LPF, &m_Hat1Buf, &m_HPF);
	
	// hihat 2
	m_Hat2WaveTable.Process(BufSize, m_Hat2Buf);
	m_Hat2Envelope.Process(BufSize, m_Hat2Buf, m_FilterCV);
	m_Hat2Filter.Process(BufSize, m_Hat2Buf, m_FilterCV, &m_LPF, &m_Hat2Buf, &m_HPF);

	// snare
	m_SnareFilterEnvelope.Process(BufSize, right, m_FilterCV);
	m_SnareWaveTable.Process(BufSize, m_SnareBuf);
	m_SnareEnvelope.Process(BufSize, m_SnareBuf, right);
	
	switch(m_FilterType)
	{
		case 0: m_SnareFilter.Process(BufSize, m_SnareBuf, m_FilterCV, &m_SnareBuf, &m_BPF, &m_HPF); break;
		case 1: m_SnareFilter.Process(BufSize, m_SnareBuf, m_FilterCV, &m_LPF, &m_SnareBuf, &m_HPF); break;
		case 2: m_SnareFilter.Process(BufSize, m_SnareBuf, m_FilterCV, &m_LPF, &m_BPF, &m_SnareBuf); break;
	}
	
	left.Mix(m_KickBuf);
	left.Mix(m_Hat1Buf);
	left.Mix(m_Hat2Buf);
	left.Mix(m_SnareBuf);

	StereoMix(left,left,right);
}

void DrumVoice::Trigger(const TriggerInfo &t)
{
	Voice::Trigger(t);
	
	static const int Drums = 4;
	
	if (((int)t.Pitch)%Drums==0)
	{
		m_KickEnvelope.Trigger(t.Time,t.Pitch, t.Volume);
		m_KickFreqEnvelope.Trigger(t.Time,t.Pitch, t.Volume);
	}
	else if (((int)t.Pitch)%Drums==1)
	{
		m_Hat1Envelope.Trigger(t.Time,t.Pitch, t.Volume);
	}
	else if (((int)t.Pitch)%Drums==2)
	{
		m_Hat2Envelope.Trigger(t.Time,t.Pitch, t.Volume);
	}
	else if (((int)t.Pitch)%Drums==3)
	{
		m_SnareEnvelope.Trigger(t.Time,t.Pitch, t.Volume);
		m_SnareFilterEnvelope.Trigger(t.Time,t.Pitch, t.Volume);
	}
}

void DrumVoice::Modify(const string &name, float v)
{ 
	map<string,int>::iterator i=m_Names.find(name);
	if (i==m_Names.end()) return;
	
	switch (i->second)
	{
		case 1: m_KickFreqEnvelope.SetDecay(v); break;
		case 2: m_KickFreqEnvelope.SetVolume(v); break;
		case 3: m_KickFreq=v; break;
		case 4: m_KickEnvelope.SetDecay(v); break;
		case 5: m_KickEnvelope.SetVolume(v); break;
		case 6: m_Hat1Envelope.SetDecay(v); break;
		case 7: m_Hat1Envelope.SetVolume(v); break;
		case 8: m_Hat1Filter.SetCutoff(v); break;
		case 9: m_Hat1Filter.SetResonance(v); break;
		case 10: m_Hat2Envelope.SetDecay(v); break;
		case 11: m_Hat2Envelope.SetVolume(v); break;
		case 12: m_Hat2Filter.SetCutoff(v); break;
		case 13: m_Hat2Filter.SetResonance(v); break;
		case 14: m_SnareEnvelope.SetDecay(v); break;
		case 15: m_SnareEnvelope.SetVolume(v); break;
		case 16: m_SnareFilterEnvelope.SetAttack(v); break;
		case 17: m_SnareFilterEnvelope.SetDecay(v); break;
		case 18: m_SnareFilterEnvelope.SetSustain(v); break;
		case 19: m_SnareFilterEnvelope.SetRelease(v); break;
		case 20: m_SnareFilterEnvelope.SetVolume(v); break;
		case 21: m_FilterType=(int)v; break;
		case 22: m_SnareFilter.SetCutoff(v); break;
		case 23: m_SnareFilter.SetResonance(v); break;
		default : cerr<<"unhandled modify code "<<i->second<<endl;
	}
}




