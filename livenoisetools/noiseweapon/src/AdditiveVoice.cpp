// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "AdditiveVoice.h"
#include <math.h>
#include <stdio.h>

using namespace std;

static const float ONE_OVER_RADCYCLE = 1.0f/((M_PI/180.0f)*360.0f);

AdditiveVoice::AdditiveVoice(int SampleRate) :
Voice(SampleRate),
m_Envelope(SampleRate),
SINCOS_LOOKUP(0)
{
	for (int n=0; n<NUM_HARMS; n++)
	{	
		m_Wave[n]=new SimpleWave(SampleRate);
		m_Level[n]=0;
	}
}

AdditiveVoice::~AdditiveVoice() 
{
	for (int n=0; n<NUM_HARMS; n++)
	{	
		delete m_Wave[n];
	}
}

void AdditiveVoice::Process(unsigned int BufSize, Sample &left, Sample &right)
{
	if (BufSize>(unsigned int)m_Buffer.GetLength())
	{
		m_Buffer.Allocate(BufSize);
	}
	left.Zero();
	for (int n=0; n<NUM_HARMS; n++)
	{		
		m_Wave[n]->Process(BufSize,left);	
	}	
	m_Envelope.Process(BufSize, left, m_Buffer);
	StereoMix(left,left,right);
}

void AdditiveVoice::Trigger(const TriggerInfo &t)
{ 
	Voice::Trigger(t);
	m_Envelope.Trigger(t.Time,t.Pitch,t.Volume);
	
	float gain=1/(float)NUM_HARMS;
	float pitch=t.Pitch/8;
	float slidepitch=t.SlidePitch/8;
	for (int n=0; n<NUM_HARMS; n++)
	{
		float vol=gain*m_Level[n];
		m_Wave[n]->Trigger(t.Time,pitch*(1<<n),slidepitch*(1<<n),vol);
	}
}

void AdditiveVoice::Modify(const string &name, float v)
{ 
	if (name=="attack") m_Envelope.SetAttack(v);
	else if (name=="decay") m_Envelope.SetDecay(v);
	else if (name=="sustain") m_Envelope.SetSustain(v);
	else if (name=="release") m_Envelope.SetRelease(v);
	else if (name=="volume") m_Envelope.SetVolume(v);
	else 
	{
		int h=(int)atof(name.c_str()+4);
		m_Level[h]=v;
	}
}




