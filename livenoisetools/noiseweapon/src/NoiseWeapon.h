// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <spiralcore/Types.h>
#include <spiralcore/ChannelHandler.h>
#include <spiralcore/Time.h>
#include <spiralcore/EventQueue.h>
#include <spiralcore/Trace.h>
#include <spiralcore/OSCServer.h>
#include "Instrument.h"

#ifndef NOISE_WEAPON
#define NOISE_WEAPON

static const int EVENT_BUFFER_SIZE=256;

class NoiseWeapon
{
public:
	NoiseWeapon(OSCServer *server, unsigned int samplerate);
	~NoiseWeapon() {}
	
private:
	static void Run(void *RunContext, unsigned int BufSize);
	void Process(unsigned int BufSize);
	void ProcessCommands();
	
	unsigned int m_SampleRate;
	
	map<int,Instrument*> m_InstrumentMap;
	
	Sample m_LeftBuffer;
	Sample m_RightBuffer;
	int    m_LeftJack;
	int    m_RightJack;
	bool 	m_Running;
	Time	m_CurrentTime;
	OSCServer *m_Server;
	EventQueue m_EventQueue;
	float m_GlobalVolume;
};

#endif
