// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <limits.h>
#include <spiralcore/JackClient.h>
#include "NoiseWeapon.h"
#include "MoogVoice.h"
#include "PhaseMod.h"
#include "DrumVoice.h"

using namespace spiralcore;

NoiseWeapon::NoiseWeapon(OSCServer *server, unsigned int samplerate) :
m_SampleRate(samplerate),
m_Running(false),
m_Server(server),
m_GlobalVolume(1.0f)
{		
	DrumVoice::RegisterNames();
	MoogVoice::RegisterNames();
	PhaseModVoice::RegisterNames();
	WaveTable::WriteWaves();

	JackClient* Audio=JackClient::Get();
 	Audio->SetCallback(Run,(void*)this);
 	Audio->Attach("noiseweapon");	

	//PortAudioClient* Audio=PortAudioClient::Get();
	//Audio->SetCallback(Run,(void*)this);
	//PortAudioClient::DeviceOptions Options;
	//Options.Samplerate=m_SampleRate;
	//Options.NumBuffers=2;
	//Options.BufferSize=512;
	//Audio->Attach("noiseweapon",Options);	
	
	m_LeftBuffer.Allocate(MAX_BUFFER);
	m_RightBuffer.Allocate(MAX_BUFFER);
	m_LeftBuffer.Zero();
	m_RightBuffer.Zero();
	
	if (Audio->IsAttached())
	{	
		//Audio->SetOutputs(m_LeftBuffer.GetNonConstBuffer(),m_RightBuffer.GetNonConstBuffer());
		
		m_LeftJack = Audio->AddOutputPort();
 		Audio->SetOutputBuf(m_LeftJack, m_LeftBuffer.GetNonConstBuffer());
 	    Audio->ConnectOutput(m_LeftJack,"alsa_pcm:playback_1");
  	    m_RightJack = Audio->AddOutputPort();
 		Audio->SetOutputBuf(m_RightJack, m_RightBuffer.GetNonConstBuffer());
  	    Audio->ConnectOutput(m_RightJack,"alsa_pcm:playback_2"); 	
 		m_Running=true;
	}
	//Sample::SetAllocator(new RealtimeAllocator(1024*1024*40));
}

void NoiseWeapon::Run(void *RunContext, unsigned int BufSize)
{ 
	((NoiseWeapon*)RunContext)->ProcessCommands();
	((NoiseWeapon*)RunContext)->Process(BufSize);
}

void NoiseWeapon::ProcessCommands()
{
	CommandRingBuffer::Command cmd;
	while (m_Server->Get(cmd))
	{
		string name = cmd.Name;
		if (name=="/modify")
		{
			map<int,Instrument*>::iterator i = m_InstrumentMap.find(cmd.GetInt(0));
			if (i!=m_InstrumentMap.end())
			{
				for (unsigned int n=1; n<cmd.Size()-1; n+=2)
				{
					i->second->Modify(cmd.GetString(n),cmd.GetFloat(n+1));
				}
			}
		}
		else if (name=="/play")
		{			
			Event e;
			e.TimeStamp.Seconds=(unsigned int)cmd.GetInt(0);
			e.TimeStamp.Fraction=(unsigned int)cmd.GetInt(1);
			e.ID=cmd.GetInt(2);
			e.Frequency=cmd.GetFloat(3);
			e.SlideFrequency=cmd.GetFloat(4);
			e.Volume=cmd.GetFloat(5);
			e.Pan=cmd.GetFloat(6);
			e.Message=(char)cmd.GetInt(7);
			e.Channel=0;

			if (e.TimeStamp.Seconds==0 && e.TimeStamp.Fraction==0)
			{
				e.TimeStamp=m_CurrentTime;
				e.TimeStamp+=0.1;
			}
			
			if (e.TimeStamp>=m_CurrentTime) 
			{
				m_EventQueue.Add(e);
				//Trace(GREEN,RED,"%f",e.TimeStamp.GetDifference(m_CurrentTime));

				if (e.TimeStamp.GetDifference(m_CurrentTime)>5)
				{
					Trace(RED,YELLOW,"Reset clock? Event far in future %f seconds",e.TimeStamp.GetDifference(m_CurrentTime));
				} 			
			}
			else 
			{
				Trace(RED,YELLOW,"Event arrived too late [%f secs], dumping",m_CurrentTime.GetDifference(e.TimeStamp));
			}	
		}
		else if (name=="/addinstrument")
		{
			map<int,Instrument*>::iterator i = m_InstrumentMap.find(cmd.GetInt(0));
			if (i==m_InstrumentMap.end())
			{
				Instrument* inst = new Instrument(cmd.GetString(1),m_SampleRate,DEF_POLY,1);
				m_InstrumentMap[cmd.GetInt(0)]=inst;
			}
		}
		else if (name=="/setclock")	
		{ 
			// baddddd :P
			Time Now;
			Now.SetToNow();
			m_CurrentTime.Seconds=Now.Seconds;
			m_CurrentTime.Fraction=Now.Fraction;		
		}
		else if (name=="/clear")	
		{ 
			for (map<int,Instrument*>::iterator i=m_InstrumentMap.begin();
				i!=m_InstrumentMap.end(); i++)
			{
				delete i->second;
			}	
			m_InstrumentMap.clear();
			Sample::GetAllocator()->Reset();
		}
		else if (name=="/globalvolume")	
		{ 		
			m_GlobalVolume=cmd.GetFloat(0);
		}
	}
}

void NoiseWeapon::Process(unsigned int BufSize)
{	
	if (BufSize==0)
	{
		cerr<<"BufSize is "<<BufSize<<" ??"<<endl;
		return;
	}

	if (BufSize>(unsigned int)m_LeftBuffer.GetLength())
	{
		m_LeftBuffer.Allocate(BufSize);
		m_RightBuffer.Allocate(BufSize);
		//PortAudioClient::Get()->SetOutputs(m_LeftBuffer.GetNonConstBuffer(),m_RightBuffer.GetNonConstBuffer());
 		JackClient::Get()->SetOutputBuf(m_LeftJack, m_LeftBuffer.GetNonConstBuffer());
 		JackClient::Get()->SetOutputBuf(m_RightJack, m_RightBuffer.GetNonConstBuffer());
	}
	
	m_LeftBuffer.Zero();
	m_RightBuffer.Zero();
	
	Time LastTime = m_CurrentTime;
	m_CurrentTime.IncBySample(BufSize,m_SampleRate);

	Event e;
	while (m_EventQueue.Get(LastTime, m_CurrentTime, e))
	{			
		for (map<int,Instrument*>::iterator i=m_InstrumentMap.begin(); 
		 		i!=m_InstrumentMap.end(); ++i)
		{ 
			if (e.ID==i->second->GetDevice())
			{
				TriggerInfo tinfo;
				tinfo.Time = LastTime.GetDifference(e.TimeStamp);
				// hack to get round bug with GetDifference throwing big numbers
				if (tinfo.Time<=0) 
				{
					tinfo.Pitch = e.Frequency;
					tinfo.SlidePitch = e.SlideFrequency;
					tinfo.Volume = e.Volume;
					tinfo.Pan = e.Pan;
					if (e.Message=='O') tinfo.Accent=true;
					else tinfo.Accent=false;
					if (e.Message=='x') tinfo.Volume=0; // note off roland stylee :)
										
					i->second->Trigger(tinfo);
				}
				else
				{
					cerr<<tinfo.Time<<endl;
					LastTime.Print();
					e.TimeStamp.Print();
					cerr<<"----------------"<<endl;
				}
			}
		}
	}
	
	for (map<int,Instrument*>::iterator i=m_InstrumentMap.begin();  i!=m_InstrumentMap.end(); ++i)
	{ 	
		i->second->Process(BufSize,m_LeftBuffer,m_RightBuffer);
	}
	
	// global volume + clip
	for (unsigned int i=0; i<BufSize; i++)
	{
		m_LeftBuffer[i]*=m_GlobalVolume;
		m_RightBuffer[i]*=m_GlobalVolume;
		
		if (m_LeftBuffer[i]<-1) m_LeftBuffer[i]=-1;
		if (m_LeftBuffer[i]>1) m_LeftBuffer[i]=1;
		if (m_RightBuffer[i]<-1) m_RightBuffer[i]=-1;
		if (m_RightBuffer[i]>1) m_RightBuffer[i]=1;
	}
}
