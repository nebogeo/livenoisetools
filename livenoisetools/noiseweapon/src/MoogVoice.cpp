// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "MoogVoice.h"

using namespace std;
map<string,int> MoogVoice::m_Names;

MoogVoice::MoogVoice(int SampleRate) :
Voice(SampleRate),
m_WaveTableA(SampleRate),
m_EnvelopeA(SampleRate),
m_WaveTableB(SampleRate),
m_EnvelopeB(SampleRate),
m_Filter(SampleRate),
m_EnvelopeF(SampleRate),
m_LFO(SampleRate),
m_FilterType(0),
m_LFOLevel(0),
m_Ring(0),
m_FilterCV(MAX_BUFFER),
m_LFOCV(MAX_BUFFER)
{
	m_LFO.SetOctave(-5);
}

void MoogVoice::RegisterNames()
{
	RegisterName("typea", m_Names);
	RegisterName("freqa", m_Names);
	RegisterName("slidea", m_Names);
	RegisterName("typeb", m_Names);
	RegisterName("freqb", m_Names);
	RegisterName("slideb", m_Names);
	RegisterName("attacka", m_Names); 
	RegisterName("decaya", m_Names);
	RegisterName("sustaina", m_Names);
	RegisterName("releasea", m_Names);
	RegisterName("volumea", m_Names); 
	RegisterName("attackb", m_Names); 
	RegisterName("decayb", m_Names);
	RegisterName("sustainb", m_Names);
	RegisterName("releaseb", m_Names);
	RegisterName("volumeb", m_Names); 
	RegisterName("attackf", m_Names); 
	RegisterName("decayf", m_Names);
	RegisterName("sustainf", m_Names);
	RegisterName("releasef", m_Names);
	RegisterName("volumef", m_Names); 
	RegisterName("ftype", m_Names);
	RegisterName("cutoff", m_Names);
	RegisterName("resonance", m_Names);
	RegisterName("ring", m_Names);
	RegisterName("lfotype", m_Names); 
	RegisterName("lfofreq", m_Names); 
	RegisterName("lfodepth", m_Names);
}

void MoogVoice::Process(unsigned int BufSize, Sample &left, Sample &right)
{
	if (BufSize>(unsigned int)m_FilterCV.GetLength())
	{
		m_FilterCV.Allocate(BufSize);
		m_LFOCV.Allocate(BufSize);
	}

	m_WaveTableA.Process(BufSize, left);
	m_EnvelopeA.Process(BufSize, left, m_FilterCV);
	m_WaveTableB.Process(BufSize, right);
	m_EnvelopeB.Process(BufSize, right,  m_FilterCV);
	
	if (m_Ring>0)
	{
		for (unsigned int i=0; i<BufSize; i++)
		{
			left[i]=((left[i]+right[i])*(1-m_Ring)) + ((left[i]*right[i])*m_Ring*10.0f);
		}	
	}
	else
	{
		left.Mix(right);
	}
	
	m_EnvelopeF.Process(BufSize, right, m_FilterCV);
	m_LFO.Process(BufSize,m_LFOCV);
	
	if (m_LFOLevel>0)
	{
		for (unsigned int i=0; i<BufSize; i++)
		{
			m_FilterCV[i]+=m_LFOCV[i]*m_LFOLevel;
		}
	}
	
	m_Filter.SetType((FilterWrapper::Type)m_FilterType);
	m_Filter.Process(BufSize, left, m_FilterCV, left);
	
	StereoMix(left,left,right);
}

void MoogVoice::Trigger(const TriggerInfo &t)
{
	Voice::Trigger(t);
	m_WaveTableA.Trigger(t.Time,t.Pitch,t.SlidePitch,0.1);
	m_EnvelopeA.Trigger(t.Time,t.Pitch,t.Volume);
	m_WaveTableB.Trigger(t.Time,t.Pitch,t.SlidePitch,0.1);
	m_EnvelopeB.Trigger(t.Time,t.Pitch,t.Volume);
	if (t.Accent) m_EnvelopeF.Trigger(t.Time,t.Pitch,0.2);
}

void MoogVoice::Modify(const string &name, float v)
{ 
	map<string,int>::iterator i=m_Names.find(name);
	if (i==m_Names.end()) return;
	
	switch (i->second)
	{
		case 1: m_WaveTableA.SetType((int)v); break;
		case 2: m_WaveTableA.SetFineFreq(v); break;
		case 3: m_WaveTableA.SetSlideLength(v); break;
		case 4: m_WaveTableB.SetType((int)v); break;
		case 5: m_WaveTableB.SetFineFreq(v); break;
		case 6: m_WaveTableB.SetSlideLength(v); break;
		case 7: m_EnvelopeA.SetAttack(v); break;
		case 8: m_EnvelopeA.SetDecay(v); break;
		case 9: m_EnvelopeA.SetSustain(v); break;
		case 10: m_EnvelopeA.SetRelease(v); break;
		case 11: m_EnvelopeA.SetVolume(v); break;
		case 12: m_EnvelopeB.SetAttack(v); break;
		case 13: m_EnvelopeB.SetDecay(v); break;
		case 14: m_EnvelopeB.SetSustain(v); break;
		case 15: m_EnvelopeB.SetRelease(v); break;
		case 16: m_EnvelopeB.SetVolume(v); break;
		case 17: m_EnvelopeF.SetAttack(v); break;
		case 18: m_EnvelopeF.SetDecay(v); break;
		case 19: m_EnvelopeF.SetSustain(v); break;
		case 20: m_EnvelopeF.SetRelease(v); break;
		case 21: m_EnvelopeF.SetVolume(v); break;
		case 22: m_FilterType=(int)v; break;
		case 23: m_Filter.SetCutoff(v); break;
		case 24: m_Filter.SetResonance(v); break;
		case 25: m_Ring=v; break;
		case 26: m_LFO.SetType((int)v); break;
		case 27: m_LFO.SetFineFreq(v); break;
		case 28: m_LFOLevel=v; break;
		default : cerr<<"unhandled modify code "<<i->second<<endl;
	}
}




