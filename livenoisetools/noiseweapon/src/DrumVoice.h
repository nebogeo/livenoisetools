// Copyright (C) 2004 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "Voice.h"
#include "Modules.h"

#ifndef DRUM_VOICE
#define DRUM_VOICE

using namespace spiralcore;

class DrumVoice : public Voice
{
public:
	DrumVoice(int SampleRate);
	virtual ~DrumVoice() {}
	
	virtual void Process(unsigned int BufSize, Sample &left, Sample &right);
	virtual void Trigger(const TriggerInfo &t);
	virtual void Modify(const string &name, float v);
	
	static void RegisterNames();

private:
	static map<string,int> m_Names;

	SimpleEnvelope  m_KickFreqEnvelope;
	SimpleEnvelope  m_KickEnvelope;
	WaveTable 		m_SnareWaveTable;
	SimpleEnvelope  m_SnareEnvelope;
	MoogFilter 		m_SnareFilter;
	Envelope  		m_SnareFilterEnvelope;
	WaveTable 		m_Hat1WaveTable;
	SimpleEnvelope  m_Hat1Envelope;
	MoogFilter 		m_Hat1Filter;
	WaveTable 		m_Hat2WaveTable;
	SimpleEnvelope  m_Hat2Envelope;
	MoogFilter 		m_Hat2Filter;
	
	int m_FilterType;
	float m_KickFreq;
	
	Sample m_FilterCV;
	Sample m_LPF;
	Sample m_BPF;
	Sample m_HPF;
	Sample m_EnvCV;
	
	Sample m_KickBuf;
	Sample m_Hat1Buf;
	Sample m_Hat2Buf;
	Sample m_SnareBuf;
	
	float m_KickTime;
	float m_Time;
};

#endif
