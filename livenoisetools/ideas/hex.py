#! /usr/bin/env python

import Tkinter
import math

		
class Hex:
	def __init__(self,master):
		self.width = 200
		self.height = 200
		self.hexwidth = 30
		self.hexheight = 30
		self.numpoints = 6
		
		self.widget = Tkinter.Canvas(master, width=self.width, height=self.height, bg='grey')
		self.buttons = []
		buttonid = 0;
		cx=self.width
		cy=self.height
		offsx = 0
		offsy = 0
		count = 0
		
		for x in range(1,50):
			print offsx,offsy
			cx=cx+(offsx*2)
			cy=cy+(offsy*2)
			p=[]
			degperp = (2*math.pi)/self.numpoints
			for i in range(0,self.numpoints+1):
				posx = math.sin(degperp*i)*(self.hexwidth/2.0)
				posy = math.cos(degperp*i)*(self.hexheight/2.0)
				p.append(cx+posx)
				p.append(cy+posy)
				if i==count%self.numpoints:
					offsx=posx
					offsy=posy
					count=count-1 
					print count
					if count<-13: count=0

			self.buttons.append(self.widget.create_polygon(p,fill="yellow"))
			self.widget.itemconfig(self.buttons[buttonid], tags=("handle"))
			self.widget.tag_bind(self.buttons[buttonid], sequence="<Button-1>", func=self.command)
			buttonid=buttonid+1	

	def command(self,event):
		cnv = event.widget
		cnv.itemconfigure (Tkinter.CURRENT, fill ="blue")
		

win = Tkinter.Tk()
slider = Hex(win)
slider.widget.pack()
win.mainloop()
