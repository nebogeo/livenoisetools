#!/usr/bin/env python

import osc
import time

oscport = 88000000

osc.Message("/nl/addtoqueue",[1,"/usr/share/sounds/question.wav"]).sendlocal(oscport)
osc.Message("/nl/loadqueue").sendlocal(oscport)

while(1):
	osc.Message("/nl/play",[1,440,0,1,0,0]).sendlocal(oscport)
	time.sleep(1)
