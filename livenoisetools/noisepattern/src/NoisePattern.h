// Copyright (C) 2003 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <list>
#include <pthread.h>
#include <spiralcore/Types.h>
#include <spiralcore/ChannelHandler.h>
#include <spiralcore/Tuna.h>
#include <spiralcore/OSCServer.h>
#include <lo/lo.h>
#include "Pattern.h"

#ifndef NOISE_PATTERN
#define NOISE_PATTERN

using namespace spiralcore;

class NoisePattern 
{
public:
	NoisePattern(OSCServer *server);
	virtual ~NoisePattern();

private:

	static void Run(NoisePattern* Context);
	void ProcessCommands();
    virtual void Process();	
		
	pthread_t  m_SeqThread;
	ChannelHandler m_CH; 
	list<PatternID> m_PatternOrder;
	map<PatternID,Pattern*> m_PatternMap;
	bool m_NoteDebug;
	bool m_Started;
	enum FilterMode{OFF,FILTER,UNFILTER};
	FilterMode m_FilterMode;
	
	vector<lo_address> m_OscAddresses;
	string m_OscEventDestination;
	
	Time m_CurrentTime;
	Time m_SeqFrom;
	static float m_TimeBuffer;
	static float m_TimeLatency;
	OSCServer *m_Server;
	
	float m_GlobalBeatTime;
	
	Tuna m_Tuna;
	bool m_QuantiseFreq;
};

#endif //  NOISE_ENGINE
