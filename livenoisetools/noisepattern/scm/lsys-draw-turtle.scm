(define lsys-draw-angle 10)
(define lsys-draw-depth 0)
(define lsys-draw-obs '())
(define lsys-draw-current-ob 0)
(define lsys-draw-current-pattern 0)
(define lsys-draw-collist '(
	#(0 0.43 0.93) #(0.31 0.8 0) #(0.31 0 0.8) #(0.8 0 0.31) #(0 0.8 0.31) #(0 0.31 0.8) 
	#(0.8 0.31 0) #(0.31 0.8 0) #(0.31 0 0.8) #(0.8 0 0.31) #(0 0.8 0.31) #(0 0.31 0.8) 
	#(0.8 0.31 0) #(0.31 0.8 0) #(0.31 0 0.8) #(0.8 0 0.31) #(0 0.8 0.31) #(0 0.31 0.8) 
	#(0.8 0.31 0) #(0.31 0.8 0) #(0.31 0 0.8) #(0.8 0 0.31) #(0 0.8 0.31) #(0 0.31 0.8) 	
	#(0.8 0.31 0) #(0.31 0.8 0) #(0.31 0 0.8) #(0.8 0 0.31) #(0 0.8 0.31) #(0 0.31 0.8) 	
	))
;(define lsys-draw-highlightcol (vector 0.93 0.43 0))
(define lsys-draw-highlightcol (vector 1 1 1))
	
(line-width 2)

(define lsys-draw-build 
	(lambda (id ch)
    	(cond 
        	((char=? ch #\+)
            	(turtle-turn (vector 0 0 lsys-draw-angle)))
        	((char=? ch #\-)
            	(turtle-turn (vector 0 0 (- lsys-draw-angle))))
        	((char=? ch #\/)
            	(turtle-turn (vector 0 lsys-draw-angle 0)))
        	((char=? ch #\\)
            	(turtle-turn (vector 0 (- lsys-draw-angle) 0)))
        	((char=? ch #\.)
             	(turtle-move 1))
       		((or (char=? ch #\o) (char=? ch #\O))
            	(turtle-vert))
        	((char=? ch #\[)
            	(set! lsys-draw-depth (+ lsys-draw-depth 1))
            	(turtle-push))
        	((char=? ch #\])
            	(cond 
                	((> lsys-draw-depth 0)
                    	(set! lsys-draw-depth (- lsys-draw-depth 1))
                    	(turtle-pop))))
			)))

(define lsys-draw-list-build 
	(lambda (id strlist)
    (lsys-draw-build id (car strlist))
    (if (eq? (cdr strlist) '())
        0
        (lsys-draw-list-build id (cdr strlist)))))

(define lsys-draw-destroy-all 
	(lambda (l)
    	(destroy (car l))
    	(if (eq? (cdr l) '())
        	0
        	(lsys-draw-destroy-all (cdr l)))))


(define lsys-draw-fix-stack
	(lambda ()
	    (if (< lsys-draw-depth 1)
	        0 
	        (begin 
	            (turtle-pop)
	            (set! lsys-draw-depth (- lsys-draw-depth 1))
	            (lsys-draw-fix-stack)))))

(define lsys-draw-highlight
	(lambda (id note)
		(if (< id (length lsys-draw-obs))
				(begin
					(grab (list-ref lsys-draw-obs id))
					;(lsys-draw-unhighlight id (pdata-size))
    				(pdata-set "c" note lsys-draw-highlightcol)
            		(ungrab)))))

(define lsys-draw-unhighlight
	(lambda (id n)
		(pdata-set "c" n (vmul (vadd 
			(vmul (list-ref lsys-draw-collist id) 0.05) 
			(pdata-get "c" n)) 
			0.9))
		(if (zero? n)
			0
			(lsys-draw-unhighlight id (- n 1)))))

(define lsys-draw-unhighlight-all
	(lambda (n)
		(grab (list-ref lsys-draw-obs n))
		(if (number? (pdata-size))
			(lsys-draw-unhighlight n (pdata-size)))
		(ungrab)
		(if (zero? n)
			0
			(lsys-draw-unhighlight-all (- n 1)))))
		
(define lsys-draw-render
	(lambda ()
		(if (not (eq? lsys-draw-obs '())) 
			(lsys-draw-unhighlight-all (- (length lsys-draw-obs) 1)))
   		(if (osc-msg "/play") 
                (begin
                    (lsys-draw-highlight (- (inexact->exact (osc 2)) 1) (inexact->exact (osc 8)))
					(lsys-draw-render))))) ; call until we run out of /plays

(define lsys-draw
	(lambda (id str type)
		(set! id (- id 1))
		(if (< (length lsys-draw-obs) (+ id 1)) 
			(set! lsys-draw-obs (append lsys-draw-obs (list 0))))
		(turtle-prim type)
		(turtle-reset)
		;(hint-none)
		(hint-wire)
		(hint-vertcols)
		(wire-colour (vector 1 1 1))
		(lsys-draw-list-build id (string->list str))
		(list-set! lsys-draw-obs id (turtle-build))
		(grab (car lsys-draw-obs))
		(recalc-normals)
		(ungrab)
		(every-frame "(lsys-draw-render)")))
	
