// Copyright (C) 2003 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <unistd.h>
#include <sys/time.h>
#include <spiralcore/NoteTable.h>
#include "KitchenSync.h"

using namespace spiralcore;

EventQueue KitchenSync::m_EventQueue;
float KitchenSync::m_Delay = 0;

KitchenSync::KitchenSync(const string &Port, const string &DestPort, const string &PlayDestPort)
{
	cerr<<"Starting kitchen sync on "<<Port<<" sending to "<<DestPort<<" and "<<PlayDestPort<<endl;
	m_Destination=lo_address_new_from_url(DestPort.c_str());
	m_PlayDestination=lo_address_new_from_url(PlayDestPort.c_str());
    m_Server = lo_server_thread_new(Port.c_str(), ErrorHandler);
    lo_server_thread_add_method(m_Server, NULL, NULL, DefaultHandler, NULL);
    lo_server_thread_add_method(m_Server, "/sync", "if", SyncHandler, this);
    lo_server_thread_add_method(m_Server, "/play", "iiiffffii", PlayHandler, this);
    lo_server_thread_add_method(m_Server, "/delay", "f", DelayHandler, this);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Audio thread side

void KitchenSync::Run()
{		
	lo_server_thread_start(m_Server);
	Time TimeNow;
	Time TimeThen;
    while (1) 
	{
		// send on those plays when the time is right...
		TimeNow.SetToNow();
		Event e;
		while (m_EventQueue.Get(TimeThen, TimeNow, e))
		{
		//	cerr<<"ks sending event..."<<endl;
			lo_send(m_PlayDestination, "/play", "iiiffffii", 
					e.TimeStamp.Seconds,
					e.TimeStamp.Fraction,
					e.ID, 
					e.Frequency, 
					e.SlideFrequency, 
					e.Volume,  
					e.Pan,
					e.Message,
					e.NoteNum);
		}
		
		TimeThen=TimeNow;
		usleep(1000); 
	}  
}

void KitchenSync::ErrorHandler(int num, const char *msg, const char *path)
{
    //cerr<<"liblo server error "<<num<<" in path "<<path<<": "<<msg<<endl;
    cerr<<"liblo server error "<<num<<endl;
}

int KitchenSync::DefaultHandler(const char *path, const char *types, lo_arg **argv, int argc, void *data, void *user_data)
{
	return 1;
}

int KitchenSync::PlayHandler(const char *path, const char *types, lo_arg **argv,
		    int argc, void *data, void *user_data)
{
	KitchenSync *server = (KitchenSync*)user_data;
	
	//cerr<<"KitchenSync::PlayHandler"<<endl;
	
	Event e;
	
	e.TimeStamp.Seconds=(unsigned int)argv[0]->i;
	e.TimeStamp.Fraction=(unsigned int)argv[1]->i;
	e.ID=argv[2]->i;
	e.Frequency=argv[3]->f;
	e.SlideFrequency=argv[4]->f;
	e.Volume=argv[5]->f;
	e.Pan=argv[6]->f;
	e.Message=(char)argv[7]->i;
	e.NoteNum=argv[8]->i;
	e.Channel=0;
	
	m_EventQueue.Add(e);
	return 1;
}

int KitchenSync::DelayHandler(const char *path, const char *types, lo_arg **argv,
		    int argc, void *data, void *user_data)
{
	KitchenSync *server = (KitchenSync*)user_data;
	server->m_Delay=argv[0]->f;
	cerr<<"delay now: "<<server->m_Delay<<endl;
	return 1;
}


int KitchenSync::SyncHandler(const char *path, const char *types, lo_arg **argv,
		    int argc, void *data, void *user_data)
{
	KitchenSync *server = (KitchenSync*)user_data;
	
	if (types[0]=='i' && types[1]=='f')
	{
		int beatsperbar = argv[0]->i;
		float beatsperminute = argv[1]->f;
		
		cerr<<"kitchensync: sync to "<<beatsperbar<<"bpb "<<beatsperminute<<"bpm"<<endl;
		Time time;
		time.SetToNow();
		time+=m_Delay;
		
		if (beatsperminute>0)
		{
			// advance to next bar sync
			time+=beatsperbar*(1/(beatsperminute/60.0f));

			time.Print();

			// send the sync out
			lo_message oscmsg=lo_message_new();
			lo_message_add_int32(oscmsg,time.Seconds); 
			lo_message_add_int32(oscmsg,time.Fraction); 
			lo_message_add_float(oscmsg,beatsperminute); 
			lo_send_message(server->m_Destination, "/sync", oscmsg);
			lo_message_free(oscmsg);
		}
	}
	return 1;
}
