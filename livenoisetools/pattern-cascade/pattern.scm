;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
; wraps all the osc messaging into easy to use (& livecode) elements

(load (full-path "lsys.scm"))

(define pattern-renderer 0)
(define pattern-type 0)
(define pattern-osc-noisepattern "4000")
(define pattern-osc-noiseweapon "4001")
(define pattern-osc-noiselab "4002")
(define pattern-osc-kitchensync "4003")
(define pattern-osc-prefix "osc.udp://localhost:")
(define pattern-osc-current "")
(define pattern-do-init 1)
(define pattern-sampledir "/home/dave/noiz/pattern-cascade/")
(define pattern-sample-id 0)

(define pattern-osc-destination
	(lambda (address)
		(if (not (string=? pattern-osc-current address)) ; minimise switches
			(osc-destination (string-append pattern-osc-prefix address)))))

(define pattern-reset
	(lambda ()
		(set! pattern-do-init 1)
		(pattern-init)))

(define pattern-init
	(lambda ()
		(set! pattern-sample-id 0)
		(pattern-sample-unmap)
		(cond ((eq? pattern-do-init 1)
			(pattern-osc-destination pattern-osc-noisepattern)
			(osc-source "4004")
			(osc-send "/clear" "" '()) 
			(osc-send "/add_dest_address" "s" (list pattern-osc-noiseweapon)) ; noiseweapon
			(osc-send "/add_dest_address" "s" (list pattern-osc-noiselab)) ; noiselab
			(osc-send "/add_dest_address" "s" (list pattern-osc-kitchensync)) ; kitchensync
			(osc-send "/add_dest_url" "s" '("/play")) 
			(osc-send "/loadtuning" "s" '("/home/dave/noiz/scales/scl/equaltemp.scl")) 
			(osc-send "/settimebuffer" "f" '(0.5)) 
			(osc-send "/settimelatency" "f" '(0.5)) 
			;(osc-send "/notedebug" "i" '(1))

			(pattern-osc-destination pattern-osc-noiseweapon)
			(osc-send "/clear" "" '())
			(osc-send "/setclock" "" '())

			(pattern-osc-destination pattern-osc-noiselab)
			(osc-send "/clear" "" '())
			(osc-send "/setclock" "" '())
			(osc-send "/addsearchpath" "s" (list pattern-sampledir))
			
			(set! pattern-do-init 0)
		))))

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
; noisepattern part

(define pattern-add
	(lambda (id)
		(pattern-osc-destination pattern-osc-noisepattern)
		(osc-send "/new" "i" (list id))
		(osc-send "/modify" "isf" (list id "Midi" id))))

(define pattern-set
	(lambda (id s)
		(pattern-osc-destination pattern-osc-noisepattern)
		(osc-send "/modifystr" "iss" (list id "Score" s))))

(define pattern-bpm
	(lambda (s)
		(pattern-osc-destination pattern-osc-noisepattern)
		(osc-send "/bpm" "f" (list s))))

(define pattern-mute
	(lambda (id s)
		(pattern-osc-destination pattern-osc-noisepattern)
		(osc-send "/modify" "isf" '(id "Playing" s))))

(define pattern-speed
	(lambda (id s)
		(pattern-osc-destination pattern-osc-noisepattern)
		(osc-send "/modify" "isf" (list id "BeatMult" s))))
		
(define pattern
	(lambda (id speed gen ls)
		(pattern-osc-destination pattern-osc-noisepattern)
		(pattern-add id)
		(pattern-speed id speed)
		(let ((str (ls-process gen ls)))
			(display str)(newline)
			(pattern-set id str)
			(if (not (number? pattern-renderer))
				(pattern-renderer id str pattern-type)))))

(define pattern-cascade-loop
	(lambda (patterns)
		(osc-send "/cascadetop" "i" (list (car patterns)))
		(if (eq? (cdr patterns) '())
			0
			(pattern-cascade-loop (cdr patterns)))))

(define pattern-cascade
	(lambda (patterns)
		(pattern-osc-destination pattern-osc-noisepattern)
		(osc-send "/muteall" "" (list))
		(if (not (eq? patterns '()))
			(pattern-cascade-loop (reverse patterns)))))
			
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
; kitchensync part

(define pattern-delay
	(lambda (s)
		(pattern-osc-destination pattern-osc-kitchensync)
		(osc-send "/delay" "f" (list s))))

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
; noiseweapon part

(define pattern-voice 
	(lambda (id type keyvalue)
		(pattern-osc-destination pattern-osc-noiseweapon)
		(osc-send "/addinstrument" "is" (list id type))
		(set! keyvalue (cons id keyvalue))
		(set! keyvalue (cons "map" keyvalue))
		(osc-send "/modify" 
			(pattern-voice-build-argsstr keyvalue "i") 
			(append (list id) keyvalue))))
		
(define pattern-voice-build-argsstr
	(lambda (keyvalue str)
		(set! str (string-append str "sf"))
		(if (eq? (cdr (cdr keyvalue)) '())
			str
			(pattern-voice-build-argsstr (cdr (cdr keyvalue)) str))))
	
(define pattern-voice-volume
	(lambda (vol)
		(pattern-osc-destination pattern-osc-noiseweapon)
		(osc-send "/globalvolume" "f" (list vol))))
	
	
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
; noiselab part

(define pattern-sample
	(lambda (id file)
		(pattern-osc-destination pattern-osc-noiselab)
		(set! pattern-sample-id (+ pattern-sample-id 1))
		(osc-send "/addtoqueue" "is" (list pattern-sample-id file))
		(osc-send "/loadqueue" "" '())
		(osc-send "/map" "ii" (list pattern-sample-id id))))

(define pattern-sample-globals 
	(lambda (id vol freq pan)
		(pattern-osc-destination pattern-osc-noiselab)
		(set! pattern-sample-id (+ pattern-sample-id 1))
		(osc-send "/setglobals" "ifff" (list vol freq pan))))

(define pattern-sample-volume
	(lambda (vol)
		(pattern-osc-destination pattern-osc-noiselab)
		(osc-send "/globalvolume" "f" (list vol))))

(define pattern-sample-pitch
	(lambda (pitch)
		(pattern-osc-destination pattern-osc-noiselab)
		(osc-send "/globalpitch" "f" (list pitch))))

(define pattern-get-samples
	(lambda (d l)
		(let ((ret (readdir d)))
			(cond ((not (string? ret))
				(closedir d)
				l)
				((if (and (> (string-length ret) 4)
						(or (string=? (substring ret (- (string-length ret) 4)) ".wav")
						    (string=? (substring ret (- (string-length ret) 4)) ".WAV")))
						(set! l (append l (list ret))))
				(pattern-get-samples d l))))))

(define pattern-samples
	(lambda (id dir)
		(pattern-osc-destination pattern-osc-noiselab)
		(pattern-sample-load-list id dir 
			(pattern-get-samples (opendir (string-append pattern-sampledir dir)) '()))
		(osc-send "/loadqueue" "" '())))

(define pattern-sample-unmap
	(lambda ()
		(pattern-osc-destination pattern-osc-noiselab)
		(osc-send "/unmapall" "" '())))
		
(define pattern-sample-load-list
	(lambda (id dir l)
		(set! pattern-sample-id (+ pattern-sample-id 1))
		(osc-send "/addtoqueue" "is" (list pattern-sample-id (string-append dir "/" (car l))))
		(osc-send "/map" "ii" (list pattern-sample-id id))
		(if (eq? (cdr l) '())
			0
			(pattern-sample-load-list id dir (cdr l)))))

(define pattern-sample-stop
	(lambda ()
		(pattern-osc-destination pattern-osc-noiselab)
		(osc-send "/stop" "" '())))
