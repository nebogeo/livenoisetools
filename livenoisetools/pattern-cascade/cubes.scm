(define cubes-angle 10)
(define cubes-depth 0)
(define cubes-obs '())
(define cubes-current-ob 0)
(define cubes-current-pattern 0)
(define cubes-collist '(
	#(0 0.43 0.93) #(0.31 0.8 0) #(0.31 0 0.8) #(0.8 0 0.31) #(0 0.8 0.31) #(0 0.31 0.8) 
	#(0.8 0.31 0) #(0.31 0.8 0) #(0.31 0 0.8) #(0.8 0 0.31) #(0 0.8 0.31) #(0 0.31 0.8) 
	#(0.8 0.31 0) #(0.31 0.8 0) #(0.31 0 0.8) #(0.8 0 0.31) #(0 0.8 0.31) #(0 0.31 0.8) 
	#(0.8 0.31 0) #(0.31 0.8 0) #(0.31 0 0.8) #(0.8 0 0.31) #(0 0.8 0.31) #(0 0.31 0.8) 	
	#(0.8 0.31 0) #(0.31 0.8 0) #(0.31 0 0.8) #(0.8 0 0.31) #(0 0.8 0.31) #(0 0.31 0.8) 	
	))
(define cubes-highlightcol (vector 0.93 0.43 0))
	
(line-width 2)

(define cubes-build 
	(lambda (id ch)
    	(cond 
        	((char=? ch #\+)
            	(translate (vector 0 1 0)))
        	((char=? ch #\-)
            	(translate (vector 0 -1 0)))
        	((char=? ch #\/)
            	(rotate (vector 0 0 1)))
        	((char=? ch #\\)
            	(rotate (vector 0 0 -1)))
        	((char=? ch #\.)
             	(translate (vector 1 0 0)))
       		((or (char=? ch #\o) (char=? ch #\O))
         		(push) 
				(if (char=? ch #\O) (scale (vector 2 2 2)))
				
            	(if (<= (length cubes-obs) id)
					(set! cubes-obs (append cubes-obs (list '()))))
					
				(let ((ob (build-cube)))	
					(list-set! cubes-obs id (append (list-ref cubes-obs id) (list ob)))
					(pop)))
        	((char=? ch #\[)
            	(set! cubes-depth (+ cubes-depth 1))
            	(push))
        	((char=? ch #\])
            	(cond 
                	((> cubes-depth 0)
                    	(set! cubes-depth (- cubes-depth 1))
                    	(pop)))))))

(define cubes-list-build 
	(lambda (id strlist)
    (cubes-build id (car strlist))
    (if (eq? (cdr strlist) '())
        0
        (cubes-list-build id (cdr strlist)))))

(define cubes-destroy-all 
	(lambda (l)
    	(destroy (car l))
    	(if (eq? (cdr l) '())
        	0
        	(cubes-destroy-all (cdr l)))))


(define cubes-fix-stack
	(lambda ()
	    (if (< cubes-depth 1)
	        0 
	        (begin 
	            (pop)
	            (set! cubes-depth (- cubes-depth 1))
	            (cubes-fix-stack)))))

(define cubes-highlight
	(lambda (id note)
		(let ((tree (list-ref cubes-obs id)))
			(if (not (null? tree))
				(begin
					(cubes-unhighlight id tree)
    				(cond
        				((< note (length tree))
            				(grab (list-ref tree note))
            				(hint-none)
							;(hint-wire)
							(hint-solid)
            				(colour cubes-highlightcol)
            				(ungrab))))))))

(define cubes-unhighlight
	(lambda (id l)
		(grab (car l))					
		(hint-none)
		(hint-wire)
		(wire-colour (vmul (list-ref cubes-collist id) 1))
		(colour (vmul (list-ref cubes-collist id) 1))
		(ungrab)
		(if (eq? (cdr l) '())
			0
			(cubes-unhighlight id (cdr l)))))

(define cubes-render
	(lambda ()
   		(if (osc-msg "/play") 
                (begin
                    (cubes-highlight (- (inexact->exact (osc 2)) 1) (inexact->exact (osc 8)))
					(cubes-render))))) ; call until we run out of /plays

(define cubes
	(lambda (id str type)
		(set! id (- id 1))
		(if (< id (length cubes-obs)) 
			(if (not (eq? (list-ref cubes-obs id) '()))
				(begin 
					(cubes-destroy-all (list-ref cubes-obs id))
					(list-set! cubes-obs id '()))))
		(push)
		(cubes-list-build id (string->list str))
		(cubes-fix-stack)
		(pop)
		(every-frame "(cubes-render)")
		))
	
(set! pattern-renderer cubes)
