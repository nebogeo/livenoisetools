// Copyright (C) 2005 Dave Griffiths
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "Tokenise.h"


vector<pair<type,string> > Tokenise(const string &in, map<int,int> &TokenLineMap)
{
	vector<pair<type,string> > out;
	unsigned int pos=0;
	int tokencount=0;
	int linecount=0;	
	bool intoken=false;
	bool incomment=false;
	bool instring=false;
	bool wasstring=false;
	string token;
	
	while (pos<in.size())
	{
		if (in[pos]=='\n') linecount++;
	
		if (!instring && (in[pos]==' ' || in[pos]=='\t' || in[pos]=='\n'))
		{
			if (in[pos]=='\n') incomment=false;
				
			if (intoken) 
			{
				if (wasstring) 
				{
					//cerr<<"STR "<<token<<endl;
					out.push_back(pair<type,string>(STR,token));					
				}
				else 
				{
					if (token[token.size()-1]==':') 
					{
						//cerr<<"LAB "<<token<<endl;
						out.push_back(pair<type,string>(LAB,token));
					}
					else 
					{
						//cerr<<"NUM "<<token<<endl;
						out.push_back(pair<type,string>(NUM,token));
					}
				}
				
				TokenLineMap[tokencount]=linecount;
				tokencount++;
				token="";
				instring=false;
				intoken=false;
				wasstring=false;
			}
		}
		else
		{
			if (in[pos]==';' && !incomment) incomment=true;
			else if (!incomment)
			{
				if (!intoken && in[pos]=='\"') instring=true;
				else
				{		
					if (in[pos]=='\"')
					{
						instring=false;
						wasstring=true;
					}
					else
					{
						intoken=true;										
						token.push_back(in[pos]);		
					}
				}
			}
		}
		pos++;
	}
	return out;
}
