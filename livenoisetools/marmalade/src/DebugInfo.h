// Copyright (C) 2005 Dave Griffiths
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <string>
#include "Stack.h"

using namespace std;

class DebugInfo
{
public:
	DebugInfo() : LineNumber(0), PC(0), StackPtr(NULL) {}
	~DebugInfo() {}
	
	int LineNumber;
	int PC;
	string Instruction;
	Stack *StackPtr;
	
	void Dump()
	{
		if (StackPtr)
		{
			cerr<<"----- "<<"line: "<<LineNumber<<":"<<PC<<" "<<Instruction<<" "<<StackPtr->Size()<<" ( ";
			StackPtr->Print();
			cerr<<")"<<endl;
			if (StackPtr->Full()) cerr<<"stack full!!!"<<endl;	
		}
	}
};
