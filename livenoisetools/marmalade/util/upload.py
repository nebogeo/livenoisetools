#!/usr/bin/env python

import sys
import osc

oscport = int(sys.argv[1])
asm = open(sys.argv[2], 'r')
microcode = asm.read()
asm.close()

osc.Message("/asm",[microcode]).sendlocal(oscport)
	
