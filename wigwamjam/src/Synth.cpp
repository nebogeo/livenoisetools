// Copyright (C) 2006 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "Synth.h"
#include <iostream>
#include <string>

using namespace std;

float TimeTerminalNode::m_Time=0;

///////////////////////////////////////////

void Synth::Build(const string &desc)
{
	if (m_Root!=NULL) Delete(m_Root);
	unsigned int pos=0;
	m_Root=BuildNode(pos,desc);
	m_Time=0;
}

void Synth::Delete(Node* node)
{
	if (node->Type()=='o')
	{	
		OpNode *opnode = (OpNode *)node;
		for (vector<Node*>::iterator i=opnode->m_Children.begin();
				i!=opnode->m_Children.end(); i++)
		{
			Delete(*i);
		}
	}
	
	delete node;
}

Node *Synth::BuildNode(unsigned int &i, const string &desc)
{
	if (i>=desc.size()) return NULL;
	if (desc[i]=='(') i++; // skip first "("
		
	string name;
	while (desc[i]!=' ' && desc[i]!=')' && i<desc.size())
	{
		name+=desc[i];
		i++;
	}
	
	Node *node=NewNode(name);
	if (node && node->Type()=='o')
	{	
		i++; // skip whitespace
		OpNode *opnode=(OpNode *)node;
		int count=0;
		while (desc[i]!=')' && i<desc.size())
		{
			opnode->AddArg(BuildNode(i,desc));
		}
		i++; // skip whitespace
	}
	
	i++;  // skip whitespace
	return node;
}

void Synth::Run(Sample &sample)
{
	if (m_Root==NULL) return;
	
	for (int n=0; n<sample.GetLength(); n++)
	{
		TimeTerminalNode::UpdateTime(m_Time);
		sample[n]=m_Root->Run();
		m_Time+=1/m_Samplerate;
	}
}

Node *Synth::NewNode(const string &name)
{
	if (name=="+") return new PlusOpNode;
	else if (name=="-") return new MinusOpNode;
	else if (name=="*") return new MultiplyOpNode;
	else if (name=="/") return new DivideOpNode;
	else if (name=="%") return new ModOpNode;
	else if (name=="sin") return new SinOpNode;
	else if (name=="cos") return new CosOpNode;
	else if (name=="time") return new TimeTerminalNode();
	else return new NumTerminalNode(strtod(name.c_str(),NULL));
}

////////////////////////////////////////////

float OpNode::RunArg(unsigned int i)
{
	if (i<m_Children.size()) 
	{
		return m_Children[i]->Run();
	}
	
	//cerr<<"not enough args for "<<i<<endl;
	return 0;
}

