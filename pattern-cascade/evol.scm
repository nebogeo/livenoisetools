(define make-desc
	(lambda (func numargs)
		(cons func numargs)))
		
(define vocab-get-func
	(lambda (desc)
		(car desc)))

(define vocab-get-numargs
	(lambda (desc)
		(cdr desc)))

(define make-random
	(lambda (vocab depth)
		(define make-arguments 
			(lambda (node n depth)
				(set! node (append node (list (make-node depth))))
				(if (zero? n)
					node
					(make-arguments node (- n 1) depth))))
		(define make-node
			(lambda (depth)		
				(if (zero? depth)
					(terminal-node)	
					(let ((desc (list-ref vocab (random (length vocab)))))
						(make-arguments (list (vocab-get-func desc)) (- (vocab-get-numargs desc) 1)
							(- depth 1))))))
							
		(define terminal-node
			(lambda ()
				(+ 1 (random 10))))
				
		(make-node depth)))
		
;(define get-subtree
;	(lambda (code depth)

(define vocab (list 
	(make-desc + 2) 
	(make-desc - 3) 
	(make-desc / 2) 
	(make-desc * 2)))

(define code (make-random vocab 6))
(display code)(newline)
(display (eval code (interaction-environment)))(newline)
