(define stroke-angle 20)
(define stroke-dist 1)
(define stroke-width 0.3)
(define stroke-obs '())
(define stroke-last '())
(define stroke-current-ob 0)
(define stroke-current-pattern 0)
(define stroke-highlightcol (vector 0.93 0.43 0))
(define stroke-textures (list 
	(load-texture "stroke/11.png")
	(load-texture "stroke/01.png") 
	(load-texture "stroke/12.png")
	(load-texture "stroke/05.png")
	(load-texture "stroke/02.png")
	(load-texture "stroke/06.png")
	(load-texture "stroke/03.png")
	(load-texture "stroke/07.png")
	(load-texture "stroke/04.png")
	(load-texture "stroke/05.png")
	(load-texture "stroke/08.png")
	(load-texture "stroke/09.png")
	(load-texture "stroke/10.png")
	))
(define stroke-curve '())

(define stroke-dir (vector 1 0 0))
(define stroke-pos (vector 0 0 0))

(define stroke-append-curve
	(lambda ()
		(set! stroke-pos (vadd stroke-pos stroke-dir))
		(set! stroke-curve (append stroke-curve (list stroke-pos)))
		(set! stroke-curve (append stroke-curve (list stroke-width)))))

(define stroke-make-curve
	(lambda (n l)
		(pdata-set "p" n (car l))
		(pdata-set "w" n (car (cdr l)))
		(if (eq? (cdr (cdr l)) '())
			0
			(stroke-make-curve (+ n 1) (cdr (cdr l))))))

(define stroke-build-curve
	(lambda ()
		(push)
			(let ((o (build-line (/ (length stroke-curve) 2))))
		(pop)
		(grab o)
			(stroke-make-curve 0 stroke-curve)
		(ungrab)
		(set! stroke-curve '())
		o)))
		
(define stroke-build 
	(lambda (id ch)
		(cond 
        	((char=? ch #\+)
            	(set! stroke-dir (vtransform stroke-dir (mrotate (vector 0 stroke-angle 0))))(stroke-append-curve))
        	((char=? ch #\-)
            	(set! stroke-dir (vtransform stroke-dir (mrotate (vector 0 (- stroke-angle) 0))))(stroke-append-curve))
        	((char=? ch #\[)
				(set! stroke-width (+ stroke-width 0.4))(stroke-append-curve))
        	((char=? ch #\])
				(set! stroke-width (- stroke-width 0.4))(stroke-append-curve))
        	((char=? ch #\/)
            	(set! stroke-dir (vtransform stroke-dir (mrotate (vector 0 0 stroke-angle))))(stroke-append-curve))
        	((char=? ch #\\)
            	(set! stroke-dir (vtransform stroke-dir (mrotate (vector 0 0 (- stroke-angle)))))(stroke-append-curve))
        	((char=? ch #\.)
				(stroke-append-curve))
       		((or (char=? ch #\o) (char=? ch #\O))
				
				; add some more points in case we haven't got any yet
				(stroke-append-curve)
				(stroke-append-curve)
				
				; make the stroke
				(let ((ob (stroke-build-curve)))	
					; add the stroke to the right list
					(list-set! stroke-obs id (append (list-ref stroke-obs id) (list ob)))
					(grab ob)
					(hide 1)
					(pdata-copy "p" "pref")
					(ungrab))
					
				(set! stroke-curve '())))))

(define stroke-list-build 
	(lambda (id strlist)
    (stroke-build id (car strlist))
    (if (eq? (cdr strlist) '())
        0
        (stroke-list-build id (cdr strlist)))))

(define stroke-destroy-all 
	(lambda (l)
    	(destroy (car l))
    	(if (eq? (cdr l) '())
        	0
        	(stroke-destroy-all (cdr l)))))

(define stroke-hide 
	(lambda (l)
    	(grab (car l))
		(hide 1)
		(ungrab)
    	(if (eq? (cdr l) '())
        	0
        	(stroke-hide (cdr l)))))

(define stroke-hide-all 
	(lambda (l)
    	(stroke-hide (car l))
    	(if (eq? (cdr l) '())
        	0
        	(stroke-hide-all (cdr l)))))

(define stroke-highlight
	(lambda (id note)
		(if (and (zero? note) (> (length stroke-obs) id)) (stroke-hide (list-ref stroke-obs id)))
		(cond ((not (eq? (list-ref stroke-last id) 0))
			(grab (list-ref stroke-last id))
			(pdata-copy "pref" "p")
			(ungrab)))
		(let ((tree (list-ref stroke-obs id)))
			(if (and (not (null? tree)) (< note (length tree)))
				(list-set! stroke-last id (list-ref tree note))))))

(define stroke-read-osc
	(lambda ()
   		(if (osc-msg "/play") 
                (begin
                    (stroke-highlight (- (inexact->exact (osc 2)) 1) (inexact->exact (osc 8)))
					(stroke-read-osc))))) ; call until we run out of /plays

(define stroke-jitter
	(lambda (n)
		(pdata-set "p" n (vadd (pdata-get "p" n) 
			(vmul (vadd (vector (flxrnd) (flxrnd) (flxrnd)) (vector -0.5 -0.5 -0.5)) 0.3)))
		(if (zero? n)
			0
			(stroke-jitter (- n 1)))))

(define stroke-animate 
	(lambda (obs)
		(cond ((not (eq? (car obs) 0))
			(grab (car obs))
			(cond ((number? (pdata-size))
				(hide 0)
				(stroke-jitter (pdata-size))))
			(ungrab)))
		(if (eq? (cdr obs) '())
			0
			(stroke-animate (cdr obs)))))

(define stroke-render
	(lambda ()
		(stroke-read-osc)
		(if (not (eq? stroke-last '())) 
			(stroke-animate stroke-last))))

(define stroke
	(lambda (id str type)
		(set! id (- id 1))
		(set! stroke-dir (vector 1 0 0))
		(set! stroke-pos (vector 0 0 0))
		(set! stroke-curve '())
		(set! stroke-width 0.3)
		
		; add to the list of lists of objects if needbe
        (cond ((<= (length stroke-obs) id)
			(set! stroke-obs (append stroke-obs (list '())))
			(set! stroke-last (append stroke-last (list 0)))))

		(if (< id (length stroke-obs)) 
			(if (not (eq? (list-ref stroke-obs id) '()))
				(begin 
					(stroke-destroy-all (list-ref stroke-obs id))
					(list-set! stroke-obs id '())
					(list-set! stroke-last id 0))))
		(push)
		(hint-unlit)
		(texture (list-ref stroke-textures (modulo id (length stroke-textures))))
		(stroke-list-build id (string->list str))
		(pop)
		(every-frame "(stroke-render)")
		))
	
(set! pattern-renderer stroke)
