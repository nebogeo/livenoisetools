(define sub-one 
    '(("freqa" 0.5 "freqb" 1.5 "typea" 3 "cutoff" 0.2 "resonance" 0.4 "lfodepth" 0.1 
       "lfofreq" 1 "decaya" 0.1 "decayb" 0.1 "slidea" 0.1 "slideb" 0.1 "mainvolume" 1)
        
     ("freqa" 1 "freqb" 1.501 "cutoff" 0.6 "resonance" 0.3 "ftype" 2 "typea" 2 "attacka" 0 
     "decaya" 0.1 "sustaina" 0.1 "releasea" 0.5 "volumea" 1 "typeb" 2 "attackb" 0 "decayb" 0.1 
     "sustainb" 0.1 "releaseb" 0.5 "volumeb" 1 "attackf" 0.2 "decayf" 0.2 "sustainf" 0.1 
     "releasef" 0.5 "volumef" 0.2 "lfodepth" 0.5 "lfofreq" 0.1 "crushfreq" 0 "crushbits" 0
      "slidea" 0.02 "slideb" 0.05 "distort" 0.7 "poly" 3)

    ("freqa" 1.0 "freqb" 1.001 "typea" 3 "cutoff" 0.1 "resonance" 0.4 "lfodepth" 0.1 
       "lfofreq" 1 "decaya" 0.04 "decayb" 0.04 "ftype" 1
      "slidea" 0.1 "slideb" 0.1 "mainvolume" 1)))

(define sub-two 
    '(("freqa" 2 "freqb" 0.5001 "typea" 3 "typeb" 3 "cutoff" 0.5 "resonance" 0.2
        "lfodepth" 0.1 "lfofreq" 1 "attacka" 0.1 "attackb" 0.2 "decaya" 0.1  "decayb" 0.5
          "slidea" 0 "slideb" 0 "poly" 4 "mainvolume" 1)
        

     ("freqa" 1 "freqb" 0.501 "cutoff" 0.2 "resonance" 0.1 "ftype" 2 "typea" 8 "attacka" 0 
      "decaya" 0.5 "sustaina" 0 "releasea" 0 "volumea" 1 "typeb" 8 "attackb" 0 "decayb" 0.2 "sustainb" 0 "releaseb" 0 "volumeb" 1
      "attackf" 0 "decayf" 0.6 "sustainf" 0 "releasef" 0 "volumef" 1 "lfodepth" 0 "lfofreq" 5 "crushfreq" 0 "crushbits" 0
      "slidea" 0 "slideb" 0.2 "distort" 0.7 "poly" 3)      

    ("freqa" 0.5 "freqb" 0.501 "typea" 3 "cutoff" 0.1 "resonance" 0.43 "lfodepth" 0.1 
       "lfofreq" 1 "decaya" 0.2 "decayb" 0.1 "ftype" 0
      "slidea" 0.1 "slideb" 0.1 "mainvolume" 1)))

(define sub-three 
    '(("freqa" 0.125 "freqb" 0.25 "typea" 3 "cutoff" 0.4 "resonance" 0.3 "decayf" 0.1 
        "lfodepth" 0 "lfofreq" 1 "decaya" 0.3 "decayb" 0.4 "volumef" 0.5
        "slidea" 0 "slideb" 0 "crushfreq" 0.9 "crushbits" 7 "mainvolume" 1)
        

     ("freqa" 2 "freqb" 0.5 "typea" 7 "typeb" 8 "cutoff" 0.1 "resonance" 0.2 
        "decayf" 0.1 "lfodepth" 0 "lfofreq" 1 "decaya" 0.2 "decayb" 0.2 
        "volumef" 1 "decayf" 1 "poly" 3 "ftype" 3 "volumea" 3 "volumeb" 3
        "slidea" 0.5 "slideb" 0.5 "crushfreq" 0 "crushbits" 0 "mainvolume" 10)
        
    ("freqa" 0.25 "freqb" 0.2501 "typea" 3 "cutoff" 0.5 "resonance" 0.45 "lfodepth" 0.1 
       "lfofreq" 1 "decaya" 0.2 "decayb" 0.1 "ftype" 2
      "slidea" 0.1 "slideb" 0.1 "mainvolume" 1)))

(define fm-one 
    '(("freq" 2 "modfreq" 0.01 "type" 2 "attack" 0 "decay" 0.1 "sustain" 0.1 
      "release" 0.5 "modtype" 0 "modattack" 0.1 "moddecay" 0.2 "modsustain" 0.1 
      "modrelease" 0.5 "fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 "fbrelease" 0.5 
      "volume" 1 "modvolume" 1 "fbvolume" 1 "crushfreq" 0 "crushbits" 0 "slide" 0 
       "modslide" 0  "poly" 3 "mainvolume" 1)
        

      ("freq" 0.5 "modfreq" 0.25 "type" 2 "attack" 0 "decay" 0.2 "sustain" 0.3 
       "release" 0.3 "modtype" 0 "modattack" 0.2 "moddecay" 0.1 "modsustain" 0.1 
       "modrelease" 0.5 "fbattack" 2.5 "fbdecay" 1 "fbsustain" 0.1 "fbrelease" 0.5 
       "volume" 1 "modvolume" 0.9 "fbvolume" 0.2 "crushfreq" 0 "crushbits" 0 "slide" 0 
        "modslide" 0  "poly" 2 "mainvolume" 1)
      
      ("freq" 0.5 "modfreq" 0.25 "type" 2 "attack" 0 "decay" 0.2 "sustain" 0.3 
       "release" 0.3 "modtype" 0 "modattack" 0.2 "moddecay" 0.1 "modsustain" 0.1 
       "modrelease" 0.5 "fbattack" 0 "fbdecay" 1 "fbsustain" 0.1 "fbrelease" 0.5 
       "volume" 1 "modvolume" 2 "fbvolume" 0.2 "crushfreq" 0.05 "crushbits" 3 
        "slide" 0 
        "modslide" 0  "poly" 2 "mainvolume" 0.7)
      ))

(define fm-two  
    '(("freq" 2 "modfreq" 1 "type" 2 "attack" 0.5 "decay" 0.1 "sustain" 0.1 "release" 0.5 
    "modtype" 0 "modattack" 0 "moddecay" 0.6 "modsustain" 0.1 "modrelease" 0.5 "fbattack" 2.5 
    "fbdecay" 1 "fbsustain" 0.1 "fbrelease" 0.5 "volume" 1 "modvolume" 0.2 "fbvolume" 0 
     "slide" 0 "modslide" 0 "poly" 3 "mainvolume" 1)

    ("freq" 1 "modfreq" 0.25 "type" 2 "attack" 0 "decay" 0.1 "sustain" 0.1 "release" 0.5 
    "modtype" 0 "modattack" 0 "moddecay" 0.1 "modsustain" 0.0 "modrelease" 0.5 
    "fbattack" 2.5 "fbdecay" 0.1 "fbsustain" 0.1 "fbrelease" 0.5 "volume" 1 
     "modvolume" 3 "fbvolume" 2 "slide" 0 "modslide" 0 "poly" 1 "mainvolume" 0.5)

    ("freq" 0.5 "modfreq" 0.25 "type" 2 "attack" 0 "decay" 0.1 "sustain" 0.2 
     "release" 0.6 "modtype" 0 "modattack" 0.2 "moddecay" 0 "modsustain" 0.1 
     "modrelease" 0.5 "fbattack" 0.5 "fbdecay" 0.1 "fbsustain" 0.1 "fbrelease" 0.5 
     "volume" 1 "modvolume" 2 "fbvolume" 6 "crushfreq" 0.5 "crushbits" 8 
     "slide" 0 
     "modslide" 0  "poly" 0 "mainvolume" 0.5)))

(pattern-voice-volume 1)
(pattern-sample-pitch 1)
(pattern-sample-volume 1)

;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(clear)
(ortho)
;(pattern-reset)
(pattern-init)

(load (full-path "stroke.scm"))

;(pattern-bpm 120)

(pattern-voice 1 "sub" (list-ref sub-one 0))
(pattern-voice 2 "sub" (list-ref sub-two 0))
(pattern-voice 3 "sub" (list-ref sub-three 0))
(pattern-voice 4 "fm" (list-ref fm-one 0))
(pattern-voice 5 "fm" (list-ref fm-two 0))
(pattern-samples 6 "atk")
(pattern-samples 7 "hrs")
(pattern-samples 8 "808")


(pattern 1 2 4 (list "!1" 
    (list 
        (list "1" "o.2+++O.")
        (list "2" "[------12]"))))

(pattern 2 1 6 (list "!1" (list (list "1" "."))))
(pattern 3 1 6 (list "!1" (list (list "1" "."))))
(pattern 4 1 6 (list "!1" (list (list "1" "."))))
(pattern 5 1 6 (list "!1" (list (list "1" "."))))
(pattern 6 1 6 (list "!1" (list (list "1" "."))))
(pattern 7 1 6 (list "!1" (list (list "1" "."))))
(pattern 8 1 6 (list "!1" (list (list "1" "."))))

(pattern-cascade '(1 2 3 4 5 6 7 8))





























