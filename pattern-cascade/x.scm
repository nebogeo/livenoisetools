(clear)
(ortho)
(line-width 4)
(pattern-init)

;(load (full-path "cubes.scm"))

(pattern-voice 1 "sub" '("typea" 2 "typeb" 2 "freqa" 0.50001 
    "cutoff" 0.1 "resonance" 0.2 "poly" 5 "mainvolume" 5 "decaya" 2 "decayb" 0.1 
        "decayf" 0.1))
(pattern 1 2 3 
    (list "!<1" 
        (list 
            (list "1" "2++1++12")
            (list "2" "o..---o.--o..O."))))


(pattern-samples 2 "kip")
(pattern 2 4 1 (list "!<111+++1" 
    (list 
        (list "1" "22[++22]")
        (list "2" "[o...o.++o..o+++.o...o..]"))))


(pattern-voice 3 "sub" '("typea" 8 "typeb" 8 
    "freqa" 1.02 "cutoff" 0.5 "resonance" 0.3 "poly" 5 "mainvolume" 5 
    "decaya" 0.1 "decayb" 0.1 "decayf" 0.1))
(pattern 3 4 3 
    (list "!<1" 
        (list 
            (list "1" "2++1++12")
            (list "2" "o..---o.--o..O."))))


(pattern-cascade '(1 3 2))
