// Copyleft (C) 2003 David Griffiths <dave@pawfal.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <python2.3/Python.h>
#include <iostream>
#include "lo/lo.h"

using namespace std;

static PyObject *OSCBlobSendError;
static PyObject *OSCBlobSendCreate( PyObject * self, PyObject *args);
static PyObject *OSCBlobSendSet( PyObject * self, PyObject *args);
static PyObject *OSCBlobSendSend( PyObject * self, PyObject *args);

float       *global_data=NULL;
unsigned int global_size=0;

static PyMethodDef OSCBlobSendMethods[] = 
{
    {"create", OSCBlobSendCreate, METH_VARARGS,"setup a float list"},
    {"set",   OSCBlobSendSet, METH_VARARGS,"set a float list"},
    {"send",  OSCBlobSendSend, METH_VARARGS,"send a float list"},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

extern "C" 
{
void initoscblobsend(void)
{
    PyObject *m, *d;
    m = Py_InitModule("oscblobsend", OSCBlobSendMethods);
    d = PyModule_GetDict(m);
    OSCBlobSendError = PyErr_NewException("oscblobsend.error", NULL, NULL);
    PyDict_SetItemString(d, "error", OSCBlobSendError);
}
}

static PyObject *OSCBlobSendCreate(PyObject * self, PyObject *args)
{ 	
	if (!PyArg_ParseTuple(args, "i", &global_size)) return NULL;
	if (global_data!=NULL) delete[] global_data;
	global_data=new float[global_size];
    Py_INCREF(Py_None);
	return Py_None;
}

static PyObject *OSCBlobSendSet(PyObject * self, PyObject *args)
{ 
	unsigned int pos;
	float value;
	if (!PyArg_ParseTuple(args, "if", &pos, &value)) return NULL;
	if (pos<global_size) global_data[pos]=value;
	else cerr<<"OSCBlobSendSet: pos out of range"<<endl;
    Py_INCREF(Py_None);
	return Py_None;
}

static PyObject *OSCBlobSendSend(PyObject * self, PyObject *args)
{ 
	char *address, *dest;
	int i;
	if (!PyArg_ParseTuple(args, "iss",&i,&address,&dest)) return NULL;

	lo_blob blob = lo_blob_new(sizeof(global_data),global_data);
    lo_address t = lo_address_new(NULL,address);
	lo_send(t,dest,"ib",i,blob);
	lo_blob_free(blob);
    Py_INCREF(Py_None);
	return Py_None;
}
