# Copyright (C) 2005 David Griffiths <dave@pawfal.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# gui tools

import Tkinter
import tkMessageBox
import tkFileDialog
import tkFont

##########################################################################

# set global apperance options
default_font = ("Courier",20,"bold")
big_font = ("Courier",40,"bold")
default_relief = Tkinter.FLAT
col_bg2     = "#cc4e00"
col_active  = "#bfda00"
col_bg      = "#000000"
default_patternwidth = 800
default_w = 800
default_h = 400

class Slider:
	def __init__(self,master,name,low,high,step,command,handlecol="white",bgcol="grey",textcol="black"):
		self.name = name
		self.low = low
		self.high = high
		self.step = step
		self.value = low
		if self.value<low: self.value=low
		if self.value>high: self.value=high
		self.command = command
		self.width = 200
		self.height = 24
		self.handlesize = self.height
		self.widget = Tkinter.Canvas(master, width=self.width, height=self.height, bg=bgcol,highlightthickness=1, highlightbackground=col_bg)
		self.handle = self.widget.create_rectangle(0,0,self.handlesize,self.handlesize,fill=handlecol,outline=handlecol)
		self.widget.itemconfig(self.handle, tags=("handle"))
		self.widget.tag_bind(self.handle, sequence="<B1-Motion>", func=self.button_callback)
		self.valuetext = self.widget.create_text(self.width/2,self.height/2,text="0",fill=textcol,font=default_font)
		self.widget.tag_bind(self.valuetext, sequence="<B1-Motion>", func=self.button_callback)
		self.widget.bind(sequence="<ButtonRelease-1>", func=self.button_callback)
		self.widget.bind(sequence="<Button-3>", func=self.button_incr_callback)
		self.update()
	
	def set(self,v):
		self.value = v
		if self.value<self.low: self.value=self.low
		if self.value>self.high: self.value=self.high
		self.update()
			
	def get(self):
		return self.value		
	
	def button_callback(self,args):
		halfwidth = self.handlesize/2
		position = args.x
		
		if position<halfwidth:
			position=halfwidth

		if position>self.width-halfwidth:
			position=self.width-halfwidth
	
		normalised = (position-halfwidth)/float(self.width-self.handlesize)
		self.value = self.low+self.calcstep(self.step,normalised*(self.high-self.low))
		self.update()
		self.command(self.value)

	def button_incr_callback(self,args):
		halfwidth = self.handlesize/2
		position = halfwidth+(self.value-self.low)/(self.high-self.low)*(self.width-self.handlesize)
		if args.x<position:
			self.value = self.value - self.step
		else:
			self.value = self.value + self.step	
		self.update()
		self.command(self.value)
		
	def update(self):	
		halfwidth = self.handlesize/2	
		position = halfwidth+(self.value-self.low)/(self.high-self.low)*(self.width-self.handlesize)
		self.widget.coords(self.handle,position-halfwidth,0,position+halfwidth,self.handlesize)	
		if type(self.step) is Tkinter.IntType:
			self.widget.itemconfig(self.valuetext,text=self.name+" "+str(int(self.value)))	
		else:
			self.widget.itemconfig(self.valuetext,text=self.name+" "+str(self.value))	
			
	def calcstep(self,s,v):
		if s < 0: s = -s
		A = round(s)
		B = 1
		while abs(s-A/B) > 4.66e-10 and B<=999999999999/10.0:
			B = B * 10
			A = round(s*B)
		if A!=0: return round(v*B/A)*A/B
		else: return v	

def Button(frame,name,command):
        return Tkinter.Button(frame, text=name, command=command, padx=1, pady=1, width=12, font=default_font, 
                        relief=default_relief, bg=col_bg2, activebackground=col_active, 
                        borderwidth=0, highlightthickness=0);

def VButton(frame,name,command):
        return Tkinter.Button(frame, text=name, command=command, font=default_font, 
                        relief=default_relief, bg=col_bg2, padx=1, pady=1, activebackground=col_active, 
                        borderwidth=0, highlightthickness=0);

def WButton(frame,name,command):
        return Tkinter.Button(frame, text=name, command=command, font=default_font, width=24,
                        relief=default_relief, bg=col_bg2, padx=1, pady=1, activebackground=col_active, 
                        borderwidth=0, highlightthickness=0);

def Entry(frame):
        return Tkinter.Entry(frame, width=2, font=default_font, relief=default_relief, 
                        bg=col_bg2, borderwidth=0, highlightthickness=0)

def BigEntry(frame):
        return Tkinter.Entry(frame, width=10, font=default_font, relief=default_relief, 
                        bg=col_bg2, borderwidth=0, highlightthickness=0)

def VBigEntry(frame):
        return Tkinter.Entry(frame, width=30, font=default_font, relief=default_relief, 
                        bg=col_bg2, borderwidth=0, highlightthickness=0)

def Checkbutton(frame,name,variable,command):
        return Tkinter.Checkbutton(frame, width=0, text=name, padx=1, pady=1,variable=variable, command=command, font=default_font, 
                        relief=default_relief, bg=col_bg2, borderwidth=0, activebackground=col_active, selectcolor=col_bg, 
                        highlightthickness=0)

def Scale(frame,label,res,mini,maxi,command):
		return Slider(frame,label,mini,maxi,res,command,col_active,col_bg2)
        #return Scale(frame, label=label, resolution=res,from_=mini, to=maxi, command=command, relief=default_relief, 
        #                bg=col_bg2, font=default_font, orient="horizontal", activebackground=col_active, borderwidth=0,
         #               sliderlength=15, troughcolor=col_bg2, highlightthickness=0)

def Listbox(frame):
        return Tkinter.Listbox(frame, font=default_font, selectmode=EXTENDED, relief=default_relief, bg=col_bg2, 
                        borderwidth=0, highlightthickness=0)

def Menubutton(frame,name):
        return Tkinter.Menubutton(frame, text=name, padx=1, pady=1,font=default_font, bg=col_bg2, relief=default_relief, activebackground=col_active)
        
def Menu(frame):
        return Tkinter.Menu(frame, font=default_font, bg=col_bg2, relief=default_relief, activebackground=col_active)

def Text(frame):
        return Tkinter.Text(frame, width=32, height=15, font=default_font, relief=default_relief, 
                        bg=col_bg2, borderwidth=0, highlightthickness=1, highlightbackground=col_bg, highlightcolor=col_bg)

def BigText(frame):
        return Tkinter.Text(frame, width=20, height=7, font=big_font, relief=default_relief, 
                        bg=col_bg2, borderwidth=0, highlightthickness=1, highlightbackground=col_bg, highlightcolor=col_bg)

class LSysEntry:
	def __init__(self,frame,name,text,command):
		self.command=command
		self.frame=Frame(frame,bg=col_bg)
		self.entry = miVBigEntry(self.frame)
		self.entry.insert(0,text)
		self.entry.bind("<Return>",command)
		self.entry.grid(column=0,row=0)
		self.mutate=miVButton(self.frame,"~",self.Mutate)
		self.mutate.grid(column=1,row=0)
		
	def Mutate(self):
		text = MutateString(self.entry.get(),0.5)
		self.entry.delete(0,"end")
		self.entry.insert(0,text)
		self.command(1)
